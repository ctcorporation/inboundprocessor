namespace NodeData.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ExtendSubjectField1 : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.TaskLists", "TL_OrginalFileName", c => c.String(maxLength: 100));
            AlterColumn("dbo.TaskLists", "TL_Subject", c => c.String(maxLength: 200));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.TaskLists", "TL_Subject", c => c.String(maxLength: 100));
            AlterColumn("dbo.TaskLists", "TL_OrginalFileName", c => c.String(maxLength: 200));
        }
    }
}
