namespace NodeData.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ChangeNullableDateTimeInTaskList : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.TaskLists", "TL_ReceiveDate", c => c.DateTime());
            AlterColumn("dbo.TaskLists", "TL_ProcessDate", c => c.DateTime());
            AlterColumn("dbo.TaskLists", "TL_P", c => c.Guid());
        }
        
        public override void Down()
        {
            AlterColumn("dbo.TaskLists", "TL_P", c => c.Guid(nullable: false));
            AlterColumn("dbo.TaskLists", "TL_ProcessDate", c => c.DateTime(nullable: false));
            AlterColumn("dbo.TaskLists", "TL_ReceiveDate", c => c.DateTime(nullable: false));
        }
    }
}
