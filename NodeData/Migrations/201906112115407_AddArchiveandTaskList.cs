namespace NodeData.Migrations
{
    using System.Data.Entity.Migrations;

    public partial class AddArchiveandTaskList : DbMigration
    {
        public override void Up()
        {


            //CreateTable(
            //    "dbo.Archives",
            //    c => new
            //        {
            //            AL_ID = c.Guid(nullable: false, identity: true),
            //            AL_TL = c.Guid(nullable: false),
            //            AL_Path = c.String(maxLength: 50),
            //            AL_ArchiveName = c.String(maxLength: 20),
            //            AL_DateArchived = c.DateTime(nullable: false),
            //            AL_FileName = c.String(maxLength: 50),
            //        })
            //    .PrimaryKey(t => t.AL_ID);

            //CreateTable(
            //    "dbo.TaskLists",
            //    c => new
            //        {
            //            TL_ID = c.Guid(nullable: false, identity: true),
            //            TL_OrginalFileName = c.String(maxLength: 100),
            //            TL_FileName = c.String(maxLength: 50),
            //            TL_ReceiveDate = c.DateTime(nullable: false),
            //            TL_Processed = c.Boolean(nullable: false),
            //            TL_Path = c.String(maxLength: 50),
            //            TL_Subject = c.String(maxLength: 100),
            //            TL_SenderID = c.String(maxLength: 50),
            //            TL_RecipientID = c.String(maxLength: 50),
            //            TL_ReceviedFolder = c.String(maxLength: 50),
            //            TL_ReceiverProcess = c.String(maxLength: 10),
            //            TL_ProcessDate = c.DateTime(nullable: false),
            //            TL_P = c.Guid(nullable: false),
            //            TL_CWFileName = c.String(maxLength: 80),
            //        })
            //    .PrimaryKey(t => t.TL_ID);

        }

        public override void Down()
        {
            DropTable("dbo.TaskLists");
            DropTable("dbo.Archives");
            DropTable("dbo.ApiLog");
        }
    }
}
