// <auto-generated />
namespace NodeData.Migrations
{
    using System.CodeDom.Compiler;
    using System.Data.Entity.Migrations;
    using System.Data.Entity.Migrations.Infrastructure;
    using System.Resources;
    
    [GeneratedCode("EntityFramework.Migrations", "6.2.0-61023")]
    public sealed partial class ExtendSubjectField : IMigrationMetadata
    {
        private readonly ResourceManager Resources = new ResourceManager(typeof(ExtendSubjectField));
        
        string IMigrationMetadata.Id
        {
            get { return "201907090508410_ExtendSubjectField"; }
        }
        
        string IMigrationMetadata.Source
        {
            get { return null; }
        }
        
        string IMigrationMetadata.Target
        {
            get { return Resources.GetString("Target"); }
        }
    }
}
