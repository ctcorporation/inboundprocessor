namespace NodeData.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ExtendSubjectField : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.TaskLists", "TL_OrginalFileName", c => c.String(maxLength: 200));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.TaskLists", "TL_OrginalFileName", c => c.String(maxLength: 100));
        }
    }
}
