namespace NodeData.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddedArchiveNameToTaskLists : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.TaskLists", "TL_ArchiveName", c => c.String(maxLength: 50));
        }
        
        public override void Down()
        {
            DropColumn("dbo.TaskLists", "TL_ArchiveName");
        }
    }
}
