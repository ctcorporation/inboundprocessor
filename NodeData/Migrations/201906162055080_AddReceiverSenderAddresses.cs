namespace NodeData.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddReceiverSenderAddresses : DbMigration
    {
        public override void Up()
        {
            //AddColumn("dbo.TaskLists", "TL_FromAddress", c => c.String(maxLength: 50));
            //AddColumn("dbo.TaskLists", "TL_ToAddress", c => c.String(maxLength: 50));
        }
        
        public override void Down()
        {
            DropColumn("dbo.TaskLists", "TL_ToAddress");
            DropColumn("dbo.TaskLists", "TL_FromAddress");
        }
    }
}
