namespace NodeData.Models
{
    using System.Data.Entity;

    public partial class NodeDataModel : DbContext
    {

        public NodeDataModel()
            : base("NodeDataEntities")
        {
        }
        public NodeDataModel(string connString)
            : base(connString)
        {

        }

        public virtual DbSet<APILOG> ApiLog { get; set; }
        public virtual DbSet<TaskList> TaskLists { get; set; }
        public virtual DbSet<Archive> Archives { get; set; }
        public virtual DbSet<vw_CustomerProfile> vw_CustomerProfile { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {

            Database.SetInitializer<NodeDataModel>(null);
            modelBuilder.Entity<APILOG>()
                .Property(e => e.F_APPCODE)
                .IsFixedLength()
                .IsUnicode(false);
        }
    }
}
