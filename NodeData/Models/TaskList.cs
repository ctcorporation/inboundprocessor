﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace NodeData.Models
{
    public class TaskList
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public Guid TL_ID { get; set; }

        [MaxLength(100)]
        public string TL_OrginalFileName { get; set; }

        [MaxLength(50)]
        public string TL_FileName { get; set; }


        public DateTime? TL_ReceiveDate { get; set; }


        public bool TL_Processed { get; set; }

        [MaxLength(50)]
        public string TL_Path { get; set; }

        [MaxLength(200)]
        public string TL_Subject { get; set; }

        [MaxLength(50)]
        public string TL_SenderID { get; set; }

        [MaxLength(50)]
        public string TL_RecipientID { get; set; }

        [MaxLength(150)]
        public string TL_ReceivedFolder { get; set; }

        [MaxLength(10)]
        public string TL_ReceiverProcess { get; set; }

        public DateTime? TL_ProcessDate { get; set; }

        public Guid? TL_P { get; set; }

        [MaxLength(80)]
        public string TL_CWFileName { get; set; }

        [MaxLength(50)]
        public string TL_FromAddress { get; set; }

        [MaxLength(50)]
        public string TL_ToAddress { get; set; }

        [MaxLength(200)]

        public string TL_ArchiveName { get; set; }



    }
}
