﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace NodeData.Models
{
   public class Archive
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public Guid AL_ID { get; set; }

        public Guid AL_TL { get; set; }

        [MaxLength(50)]
        public string AL_Path { get; set; }

        [MaxLength(20)]
        public string AL_ArchiveName { get; set; }


        public DateTime AL_DateArchived { get; set; }

        [MaxLength(50)]
        public string AL_FileName { get; set; }



    }
}
