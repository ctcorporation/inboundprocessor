namespace NodeData.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class APILOG
    {
        [Required]
        [StringLength(80)]
        public string F_FILENAME { get; set; }

        [Required]
        [StringLength(15)]
        public string F_SENDERID { get; set; }

        [StringLength(20)]
        public string F_CLIENTID { get; set; }

        [StringLength(3)]
        public string F_APPCODE { get; set; }

        [StringLength(50)]
        public string F_SUBJECT { get; set; }

        [Key]
        public Guid F_TRACKINGID { get; set; }

        [StringLength(80)]
        public string F_CWFILENAME { get; set; }

        public DateTime? F_DATERECEIVED { get; set; }
    }
}
