﻿using NodeData;
using NodeData.DTO;
using NodeData.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Xml.Linq;
using XMLLocker.Cargowise;

namespace InboundProcessor.Classes
{
    public class IdentifyFile
    {

        #region members
        string _connstring;
        string _xmlFile;
        string _errorString;
        //Cargowise possible XML Types
        private readonly List<string> cwTypes = new List<string> { "UDM", "NDM", "XMS" };
        #endregion

        #region properties

        public string ErrorString
        {
            get
            { return _errorString; }
            set
            { _errorString = value; }
        }
        public string Connstring
        {
            get
            {
                return _connstring;
            }
            set
            {
                _connstring = value;

            }
        }
        public string XmlFile
        {
            get
            {
                return _xmlFile;
            }
            set
            {
                _xmlFile = value;
            }

        }

        #endregion

        #region constructors
        public IdentifyFile(string connString)
        {
            Connstring = connString;
        }
        #endregion

        #region Helpers

        #endregion

        #region methods


        public ApiLog IdFile(string xmlFile)
        {
            if (!File.Exists(xmlFile))
            {
                _errorString += "Xml file: " + xmlFile + " no longer exists";
                return null;
            }
            var appType = CWFunctions.GetXMLType(xmlFile);
            if (!cwTypes.Contains(appType))
            {
                _errorString += "XML File is not a Cargowise File and cannot be identified as yet";
                return null;
            }
            var senderId = string.Empty;
            var recipientId = string.Empty;
            var xDoc = XDocument.Load(xmlFile);
            var ns = xDoc.Root.Name.Namespace;
            var elSenderID = xDoc.Descendants().Where(n => n.Name == ns + "SenderID").FirstOrDefault();
            if (elSenderID != null)
            {
                senderId = elSenderID.Value;
            }
            else
            {

            }
            var elRecipientID = xDoc.Descendants().Where(n => n.Name == ns + "RecipientID").FirstOrDefault();
            if (elRecipientID != null)
            {
                recipientId = elRecipientID.Value;
            }
            var apiLog = new ApiLog();
            using (IUnitOfWork uow = new UnitOfWork(new NodeDataContext(_connstring)))
            {
                apiLog = (new ApiLog
                {
                    F_APPCODE = appType,
                    F_TRACKINGID = Guid.NewGuid(),
                    F_CLIENTID = recipientId,
                    F_SENDERID = senderId,
                    F_SUBJECT = string.Empty,
                    F_FILENAME = Path.GetFileName(xmlFile),
                    F_CWFILENAME = string.Empty,
                    F_DATERECEIVED = DateTime.Now
                });
                uow.ApiLogs.Add(apiLog);
                uow.Complete();
            }

            return apiLog;
        }
        #endregion

    }
}
