﻿using NodeData;
using NodeData.DTO;
using NodeData.Models;
using System.Linq;

namespace InboundProcessor.Classes
{
    public class ApplicationSettings
    {
        #region members
        AppSetting _appSetting;
        #endregion

        #region Properties

        #endregion

        #region Constructors
        public ApplicationSettings(string context)
        {
            using ( IUnitOfWork uow = new UnitOfWork(new NodeDataContext(context)))
            {

                AppSetting appSetting =uow.AppSettings.Find(x => x.AS_AppName == "").FirstOrDefault();
                if ( appSetting==null)
                {
                    appSetting = new AppSetting
                    {
                        AS_AppName = ""

                    };
                    uow.AppSettings.Add(appSetting);
                    uow.Complete();
                }
                _appSetting = appSetting;
            }

        }
        #endregion

        #region Methods

        #endregion

        #region Helpers

        #endregion

    }
}
