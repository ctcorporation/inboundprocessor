﻿using NodeData;
using NodeData.DTO;
using NodeData.Models;
using System.Collections.Generic;
using System.Linq;

namespace InboundProcessor
{
    class ProfileListBuilder
    {

        #region Fields
        string _connstring;
        #endregion

        #region Properties
        public string ConnString
        {
            get
            { return this._connstring; }
            set
            { this._connstring = value; }

        }

        #endregion

        #region Constructors
        public ProfileListBuilder()
        {

        }

        public ProfileListBuilder(string connString)
        {
            this._connstring = connString;
        }

        #endregion

        #region Methods

        #endregion

        #region Helpers

        #endregion

        public List<vw_CustomerProfile> BuildList(string deliveryType, string direction, string active)
        {
            using (IUnitOfWork uow = new UnitOfWork(new NodeDataContext(_connstring)))
            {
                var pList = uow.CustProfiles.Find(x => x.P_DIRECTION == direction
                                            && x.P_DELIVERY == deliveryType
                                            && x.P_ACTIVE == active
                                            ).OrderBy(x => x.P_DESCRIPTION).ToList();
                if (pList.Count == 0)
                {
                    return null;

                }
                else
                {
                    return pList;
                }
            }
        }
        public List<vw_CustomerProfile> BuildList(string deliveryType, string direction, string active, string msgTypefilter)
        {
            using (IUnitOfWork uow = new UnitOfWork(new NodeDataContext(_connstring)))
            {
                var pList = uow.CustProfiles.Find(x => x.P_DIRECTION == direction
                                            && x.P_DELIVERY == deliveryType
                                            && x.P_ACTIVE == active
                                            && x.P_MSGTYPE.StartsWith(msgTypefilter) 
                                            ).OrderBy(x => x.P_DESCRIPTION).ToList();
                //if (!string.IsNullOrEmpty(msgTypefilter))
                //{
                //    pList = (from p in pList
                //             where p.P_MESSAGETYPE.StartsWith(msgTypefilter)
                //             select p).ToList();
                //}
                if (pList.Count == 0)
                {
                    return null;
                }
                else
                {
                    return pList;
                }
            }
        }
    }
}
