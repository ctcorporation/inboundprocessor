﻿using NodeData;
using NodeData.DTO;
using NodeData.Models;
using System;
using System.IO;
using System.Windows.Forms;
using System.Xml.Linq;

namespace InboundProcessor.Views
{
    public partial class Settings : Form
    {
        public Settings()
        {
            InitializeComponent();
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void tableLayoutPanel2_Paint(object sender, PaintEventArgs e)
        {
        }

        private void Settings_Load(object sender, EventArgs e)
        {
            if (Globals.MailServerSettings != null)
            {
                txtSmtpServer.Text = Globals.MailServerSettings.Server;
                txtSmtpUserName.Text = Globals.MailServerSettings.UserName;
                txtSmtpPassword.Text = Globals.MailServerSettings.Password;
                txtSmtpPort.Text = Globals.MailServerSettings.Port.ToString();
                cbSMTPSecure.Checked = Globals.MailServerSettings.IsSecure;
                txtSmtpEmailAddress.Text = Globals.MailServerSettings.Email;
            }
            if (Globals.ConnString != null)
            {
                txtDatabaseName.Text = Globals.ConnString.DatabaseName;
                txtDatabaseServer.Text = Globals.ConnString.ServerName;
                txtUserName.Text = Globals.ConnString.User;
                txtPassword.Text = Globals.ConnString.Password;
            }
            edWinSCP.Text = Globals.WinSCPLocation;
            txtTestTempLocation.Text = Globals.TestTempLocation;
            txtTestPrimaryLocation.Text = Globals.TestPrimaryLocation;
            txtTestArchiveLocation.Text = Globals.TestArchiveLocation;
            txtReProcessLoc.Text = Globals.ReProcessLocation;
            txtTestReProcessLocation.Text = Globals.TestReProcessLocation;
            txtTempLocation.Text = Globals.TempLocation;
            txtPrimaryLocation.Text = Globals.PrimaryLocation;
            txtArchiveLocation.Text = Globals.ArchiveLocation;
            txtTestPickupLocation.Text = Globals.TestPickupLocation;
            txtPickupLocation.Text = Globals.PickupLocation;
            txtAlertsTo.Text = Globals.AlertsTo;
            txtPollTime.Value = Globals.PollTime;
            txtRefresh.Value = Globals.RefreshTime;
        }

        private void CreatePath(string path)
        {
            try
            {
                if (!Directory.Exists(path))
                {
                    string rootPath = Path.Combine(path);
                    Directory.CreateDirectory(rootPath);
                }
            }
            catch (UnauthorizedAccessException)
            {
                MessageBox.Show("You do not have access to create a folder here.Please Check", "Folder Write Access Denied", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
            catch (IOException exIO)
            {
                MessageBox.Show("Error Creating Directory: " + exIO.Message, "Folder Write Access Denied", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
        }



        private void btnSave_Click(object sender, EventArgs e)
        {
            XDocument doc = XDocument.Load(Globals.AppConfig);
            string section = "Database";
            var node = doc.Root.Element(section)
                        .Element("ServerName");
            if (node == null)
            {
                XElement el = new XElement("ServerName");
                el.Value = txtDatabaseServer.Text;
                XElement xNewnode = doc.Root.Element(section);
                xNewnode.Add(el);
            }
            else
            {
                node.Value = txtDatabaseServer.Text;
            }
            node = doc.Root.Element(section)
                        .Element("DatabaseName");
            if (node == null)
            {
                XElement el = new XElement("DatabaseName");
                el.Value = txtDatabaseName.Text;
                XElement xNewnode = doc.Root.Element(section);
                xNewnode.Add(el);
            }
            else
            {
                node.Value = txtDatabaseName.Text;
            }
            node = doc.Root.Element(section)
                        .Element("UserName");
            if (node == null)
            {
                XElement el = new XElement("UserName");
                el.Value = txtUserName.Text;
                XElement xNewnode = doc.Root.Element(section);
                xNewnode.Add(el);
            }
            else
            {
                node.Value = txtUserName.Text;
            }

            node = doc.Root.Element(section)
                        .Element("Password");
            if (node == null)
            {
                XElement el = new XElement("Password");
                el.Value = txtPassword.Text;
                XElement xNewnode = doc.Root.Element(section);
                xNewnode.Add(el);
            }
            else
            {
                node.Value = txtPassword.Text;
            }
            section = "Communications";
            node = doc.Root.Element(section)
                         .Element("SMTPServer");
            if (node == null)
            {
                XElement el = new XElement("SMTPServer");
                el.Value = txtSmtpServer.Text;
                XElement xNewnode = doc.Root.Element(section);
                xNewnode.Add(el);
            }
            else
            {
                node.Value = txtSmtpServer.Text;
            }

            node = doc.Root.Element(section)
             .Element("SMTPUser");
            if (node == null)
            {
                XElement el = new XElement("SMTPUser");
                el.Value = txtSmtpUserName.Text;
                XElement xNewnode = doc.Root.Element(section);
                xNewnode.Add(el);
            }
            else
            {
                node.Value = txtSmtpUserName.Text;
            }

            node = doc.Root.Element(section)
             .Element("SMTPEmailFrom");
            if (node == null)
            {
                XElement el = new XElement("SMTPEmailFrom");
                el.Value = txtSmtpEmailAddress.Text;
                XElement xNewnode = doc.Root.Element(section);
                xNewnode.Add(el);
            }
            else
            {
                node.Value = txtSmtpEmailAddress.Text;
            }
            node = doc.Root.Element(section)
                         .Element("SMTPPassword");
            if (node == null)
            {
                XElement el = new XElement("SMTPPassword");
                el.Value = txtSmtpPassword.Text;
                XElement xNewnode = doc.Root.Element(section);
                xNewnode.Add(el);
            }
            else
            {
                node.Value = txtSmtpPassword.Text;
            }

            node = doc.Root.Element(section)
                         .Element("SMTPPort");
            if (node == null)
            {
                XElement el = new XElement("SMTPPort");
                el.Value = txtSmtpPort.Text;
                XElement xNewnode = doc.Root.Element(section);
                xNewnode.Add(el);
            }
            else
            {
                node.Value = txtSmtpPort.Text;
            }

            node = doc.Root.Element(section)
                         .Element("SMTPSsl");
            if (node == null)
            {
                XElement el = new XElement("SMTPSsl");
                el.Value = cbSMTPSecure.Checked.ToString();
                XElement xNewnode = doc.Root.Element(section);
                xNewnode.Add(el);
            }
            else
            {
                node.Value = cbSMTPSecure.Checked.ToString();
            }
            section = "Config";

            node = doc.Root.Element(section)
                         .Element("WinSCPLocation");
            CreatePath(edWinSCP.Text);
            if (node == null)
            {
                XElement el = new XElement("WinSCPLocation");
                el.Value = edWinSCP.Text;
                XElement xNewnode = doc.Root.Element(section);
                xNewnode.Add(el);
            }
            else
            {
                node.Value = edWinSCP.Text;
            }

            node = doc.Root.Element(section)
                         .Element("PrimaryLocation");
            CreatePath(txtPrimaryLocation.Text);
            if (node == null)
            {
                XElement el = new XElement("PrimaryLocation");
                el.Value = txtPrimaryLocation.Text;
                XElement xNewnode = doc.Root.Element(section);
                xNewnode.Add(el);
            }
            else
            {
                node.Value = txtPrimaryLocation.Text;
            }

            node = doc.Root.Element(section)
                         .Element("OutboundLocation");
            CreatePath(txtOutboundLocation.Text);
            if (node == null)
            {
                XElement el = new XElement("OutboundLocation");
                el.Value = txtOutboundLocation.Text;
                XElement xNewnode = doc.Root.Element(section);
                xNewnode.Add(el);
            }
            else
            {
                node.Value = txtOutboundLocation.Text;
            }

            node = doc.Root.Element(section)
                         .Element("ArchiveLocation");
            CreatePath(txtArchiveLocation.Text);
            if (node == null)
            {
                XElement el = new XElement("ArchiveLocation");
                el.Value = txtArchiveLocation.Text;
                XElement xNewnode = doc.Root.Element(section);
                xNewnode.Add(el);
            }
            else
            {
                node.Value = txtArchiveLocation.Text;
            }

            node = doc.Root.Element(section)
                         .Element("TemporaryLocation");
            CreatePath(txtTempLocation.Text);
            if (node == null)
            {
                XElement el = new XElement("TemporaryLocation");
                el.Value = txtTempLocation.Text;
                XElement xNewnode = doc.Root.Element(section);
                xNewnode.Add(el);
            }
            else
            {
                node.Value = txtTempLocation.Text;
            }

            node = doc.Root.Element(section)
                         .Element("ReProcessLocation");
            CreatePath(txtReProcessLoc.Text);
            if (node == null)
            {
                XElement el = new XElement("ReProcessLocation");
                el.Value = txtReProcessLoc.Text;
                XElement xNewnode = doc.Root.Element(section);
                xNewnode.Add(el);
            }
            else
            {
                node.Value = txtReProcessLoc.Text;
            }

            node = doc.Root.Element(section)
                         .Element("TestReProcessLocation");
            CreatePath(txtTestReProcessLocation.Text);
            if (node == null)
            {
                XElement el = new XElement("TestReProcessLocation");
                el.Value = txtTestReProcessLocation.Text;
                XElement xNewnode = doc.Root.Element(section);
                xNewnode.Add(el);
            }
            else
            {
                node.Value = txtTestReProcessLocation.Text;
            }

            node = doc.Root.Element(section)
                         .Element("TestPrimaryLocation");
            CreatePath(txtTestPrimaryLocation.Text);
            if (node == null)
            {
                XElement el = new XElement("TestPrimaryLocation");
                el.Value = txtTestPrimaryLocation.Text;
                XElement xNewnode = doc.Root.Element(section);
                xNewnode.Add(el);
            }
            else
            {
                node.Value = txtTestPrimaryLocation.Text;
            }

            node = doc.Root.Element(section)
                         .Element("TestOutboundLocation");
            CreatePath(txtTestOutboundLocation.Text);
            if (node == null)
            {
                XElement el = new XElement("TestOutboundLocation");
                el.Value = txtTestOutboundLocation.Text;
                XElement xNewnode = doc.Root.Element(section);
                xNewnode.Add(el);
            }
            else
            {
                node.Value = txtTestOutboundLocation.Text;
            }

            node = doc.Root.Element(section)
                         .Element("TestArchiveLocation");
            CreatePath(txtTestArchiveLocation.Text);
            if (node == null)
            {
                XElement el = new XElement("TestArchiveLocation");
                el.Value = txtTestArchiveLocation.Text;
                XElement xNewnode = doc.Root.Element(section);
                xNewnode.Add(el);
            }
            else
            {
                node.Value = txtTestArchiveLocation.Text;
            }

            node = doc.Root.Element(section)
                         .Element("TestTemporaryLocation");
            CreatePath(txtTestTempLocation.Text);
            if (node == null)
            {
                XElement el = new XElement("TestTemporaryLocation");
                el.Value = txtTestTempLocation.Text;
                XElement xNewnode = doc.Root.Element(section);
                xNewnode.Add(el);
            }
            else
            {
                node.Value = txtTestTempLocation.Text;
            }

            node = doc.Root.Element(section)
                         .Element("TestPickupLocation");
            CreatePath(txtTestPickupLocation.Text);
            if (node == null)
            {
                XElement el = new XElement("TestPickupLocation");
                el.Value = txtTestPickupLocation.Text;
                XElement xNewnode = doc.Root.Element(section);
                xNewnode.Add(el);
            }
            else
            {
                node.Value = txtTestPickupLocation.Text;
            }

            node = doc.Root.Element(section)
                         .Element("PickupLocation");
            CreatePath(txtPickupLocation.Text);
            if (node == null)
            {
                XElement el = new XElement("PickupLocation");
                el.Value = txtPickupLocation.Text;
                XElement xNewnode = doc.Root.Element(section);
                xNewnode.Add(el);
            }
            else
            {
                node.Value = txtPickupLocation.Text;
            }

            node = doc.Root.Element(section)
                         .Element("PollTime");
            if (node == null)
            {
                XElement el = new XElement("PollTime");
                el.Value = txtPollTime.Text;
                XElement xNewnode = doc.Root.Element(section);
                xNewnode.Add(el);
            }
            else
            {
                node.Value = txtPollTime.Text;
            }

            node = doc.Root.Element(section)
                         .Element("RefreshTime");
            if (node == null)
            {
                XElement el = new XElement("RefreshTime");
                el.Value = txtRefresh.Text;
                XElement xNewnode = doc.Root.Element(section);
                xNewnode.Add(el);
            }
            else
            {
                node.Value = txtRefresh.Text;
            }
            node = doc.Root.Element(section)
                .Element("AlertsTo");
            if (node == null)
            {
                XElement el = new XElement("AlertsTo");
                el.Value = txtAlertsTo.Text;
                XElement xNewNode = doc.Root.Element(section);
                xNewNode.Add(el);
            }
            else
            {
                node.Value = txtAlertsTo.Text;
            }

            doc.Save(Globals.AppConfig);
            MessageBox.Show("Application Settings saved.");

        }

        private void btnTest_Click(object sender, EventArgs e)
        {
            txtConnectionResults.Text = "";
            NodeData.IConnectionManager cm = new ConnectionManager(
                txtDatabaseServer.Text,
                txtDatabaseName.Text,
                txtUserName.Text,
                txtPassword.Text
            );
            string connstring = cm.ConnString;


            using (IUnitOfWork uow = new UnitOfWork(new NodeDataContext(connstring)))
            {
                try
                {
                    if (uow.DBExists())
                    {
                        txtConnectionResults.Text += "Database Connection Ok.";
                    }
                }
                catch (Exception ex)
                {
                    txtConnectionResults.Text += "Error Connecting to the Database. Error was :" + ex.GetType().Name + Environment.NewLine + "Error details: " + ex.Message;
                }
            }

        }

        private void btnTestTempLocation_Click(object sender, EventArgs e)
        {
            FolderBrowserDialog fd = new FolderBrowserDialog();
            fd.SelectedPath = txtTestTempLocation.Text;
            fd.ShowDialog();
            txtTestTempLocation.Text = fd.SelectedPath;
        }

        private void btnTestPrimaryLocation_Click(object sender, EventArgs e)
        {
            FolderBrowserDialog fd = new FolderBrowserDialog();
            fd.SelectedPath = txtTestPrimaryLocation.Text;
            fd.ShowDialog();
            txtTestPrimaryLocation.Text = fd.SelectedPath;
        }

        private void btnTestArchive_Click(object sender, EventArgs e)
        {
            FolderBrowserDialog fd = new FolderBrowserDialog();
            fd.SelectedPath = txtTestArchiveLocation.Text;
            fd.ShowDialog();
            txtTestArchiveLocation.Text = fd.SelectedPath;
        }

        private void btnTempLocation_Click(object sender, EventArgs e)
        {
            FolderBrowserDialog fd = new FolderBrowserDialog();
            fd.SelectedPath = txtTempLocation.Text;
            fd.ShowDialog();
            txtTempLocation.Text = fd.SelectedPath;
        }

        private void btnPrimaryLocation_Click(object sender, EventArgs e)
        {
            FolderBrowserDialog fd = new FolderBrowserDialog();
            fd.SelectedPath = txtPrimaryLocation.Text;
            fd.ShowDialog();
            txtPrimaryLocation.Text = fd.SelectedPath;
        }

        private void btnArchiveLocation_Click(object sender, EventArgs e)
        {
            FolderBrowserDialog fd = new FolderBrowserDialog();
            fd.SelectedPath = txtArchiveLocation.Text;
            fd.ShowDialog();
            txtArchiveLocation.Text = fd.SelectedPath;
        }

        private void btnTestPickupLocation_Click(object sender, EventArgs e)
        {
            FolderBrowserDialog fd = new FolderBrowserDialog();
            fd.SelectedPath = txtTestPickupLocation.Text;
            fd.ShowDialog();
            txtTestPickupLocation.Text = fd.SelectedPath;
        }

        private void btnPickupLocation_Click(object sender, EventArgs e)
        {
            FolderBrowserDialog fd = new FolderBrowserDialog();
            fd.SelectedPath = txtPickupLocation.Text;
            fd.ShowDialog();
            txtPickupLocation.Text = fd.SelectedPath;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            FolderBrowserDialog fd = new FolderBrowserDialog();
            fd.SelectedPath = edWinSCP.Text;
            fd.ShowDialog();
            edWinSCP.Text = fd.SelectedPath;

        }

        private void btnOutboundLocation_Click(object sender, EventArgs e)
        {
            FolderBrowserDialog fd = new FolderBrowserDialog();
            fd.SelectedPath = txtOutboundLocation.Text;
            fd.ShowDialog();
            txtOutboundLocation.Text = fd.SelectedPath;
        }

        private void btnTestOutboundLocation_Click(object sender, EventArgs e)
        {
            FolderBrowserDialog fd = new FolderBrowserDialog();
            fd.SelectedPath = txtTestOutboundLocation.Text;
            fd.ShowDialog();
            txtTestOutboundLocation.Text = fd.SelectedPath;
        }
    }
}
