﻿using InboundProcessor.Classes;
using InboundProcessor.Views;
using NodeData;
using NodeData.DTO;
using NodeData.Models;
using NodeResources;
using SendModule;
using System;
using System.Collections.Generic;
using System.Data.Entity.Validation;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Timers;
using System.Windows.Forms;
using System.Xml;
using System.Xml.Linq;
using XMLLocker.CTC;

namespace InboundProcessor
{

    public partial class MainForm : Form
    {
        System.Timers.Timer _mainTimer;
        System.Timers.Timer _refreshTimer;
        System.Timers.Timer _highTideTimer;
        Stopwatch eventStopWatch;
        int _logLevel;
        bool CheckStopped;
        public MainForm()
        {
            InitializeComponent();
            _mainTimer = new System.Timers.Timer
            {
                Interval = (int)TimeSpan.FromMinutes(5).TotalMilliseconds
            };
            _mainTimer.Elapsed += new ElapsedEventHandler(TimerMain_Elapsed);
            _refreshTimer = new System.Timers.Timer
            {
                Interval = (int)TimeSpan.FromMinutes(5).TotalMilliseconds
            };


            _highTideTimer = new System.Timers.Timer
            {
                Interval = (int)TimeSpan.FromMinutes(15).TotalMilliseconds
            };
            _highTideTimer.Elapsed += new ElapsedEventHandler(HighTideTimer_Elapsed);

        }

        private int GetLogLevel()
        {
            foreach (RadioButton rb in gbLogging.Controls)
            {
                if (rb.Checked)
                {
                    return int.Parse(rb.Tag.ToString());
                }
            }
            return -1;
        }

        private void HighTideTimer_Elapsed(object sender, ElapsedEventArgs e)
        {
            if (eventStopWatch.IsRunning)
            {
                if (eventStopWatch.Elapsed.TotalMinutes >= 5)
                {

                    if (GetLogLevel() > 0)
                    {
                        using (AppLogging log = new AppLogging(Globals.ConnString.ConnString))
                        {

                            MethodBase m = MethodBase.GetCurrentMethod();
                            log.AddLog(new AppLog
                            {

                                GL_AppName = Assembly.GetExecutingAssembly().GetName().Name,
                                GL_ServerName = Environment.MachineName.ToString(),
                                GL_Date = DateTime.Now,
                                GL_ProcName = m.Name,
                                GL_MessageType = "Warning",
                                GL_MessageLevel = 1,
                                GL_MessageDetail = "Event timer found to be running for " + eventStopWatch.Elapsed.TotalMinutes + " Minutes"
                            });

                        }
                    }



                    btnStart_Click(btnStart, new EventArgs());
                    System.Threading.Thread.Sleep(1000);
                    btnStart_Click(btnStart, new EventArgs());
                }
            }
        }





        private void TimerMain_Elapsed(object sender, ElapsedEventArgs e)
        {

            ProcessQueues();
            ReProcessQueue();

        }

        private void ReProcessQueue()
        {
            MethodBase m = MethodBase.GetCurrentMethod();
            using (IHeartBeatManager heartBeat = new HeartBeatManager(Globals.ConnString.ConnString))
            {

                heartBeat.ExeName = Assembly.GetExecutingAssembly().GetName().Name;
                heartBeat.ExePath = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData),
                    System.Reflection.Assembly.GetExecutingAssembly().GetName().Name);
                heartBeat.RegisterHeartBeat("CTC", m.Name, m.GetParameters());
            }

            _mainTimer.Stop();
            string reProcLoc;
            reProcLoc = tslMode.Text == "Production" ? Globals.ReProcessLocation : Globals.TestReProcessLocation;
            string path = tslMode.Text == "Production" ? Globals.PickupLocation : Globals.TestPickupLocation;
            using (IUnitOfWork uow = new UnitOfWork(new NodeDataContext(Globals.ConnString.ConnString)))
            {
                if (eventStopWatch == null)
                {
                    eventStopWatch = new Stopwatch();
                }
                eventStopWatch.Reset();
                eventStopWatch.Start();
                int fCount = 0;
                DirectoryInfo dir = new DirectoryInfo(reProcLoc);
                foreach (FileInfo file in dir.GetFiles())
                {
                    //Check CTC Filename in tasklist
                    var fileFound = uow.TaskLists.Find(x => x.TL_FileName == file.Name || x.TL_CWFileName == file.Name).FirstOrDefault();

                    if (fileFound != null)
                    {
                        try
                        {
                            if (!File.Exists(Path.Combine(path, file.Name)))
                            {
                                File.Move(file.FullName, Path.Combine(path, file.Name));
                            }

                            TaskList tl = new TaskList
                            {
                                TL_ID = new Guid(),
                                TL_P = fileFound.TL_P,
                                TL_CWFileName = fileFound.TL_CWFileName,
                                TL_ArchiveName = fileFound.TL_ArchiveName,
                                TL_FileName = fileFound.TL_FileName,
                                TL_FromAddress = fileFound.TL_FromAddress,
                                TL_OrginalFileName = fileFound.TL_OrginalFileName,
                                TL_Path = path,
                                TL_Processed = false,
                                TL_ReceiveDate = fileFound.TL_ReceiveDate,
                                TL_ReceivedFolder = fileFound.TL_ReceivedFolder,
                                TL_ReceiverProcess = fileFound.TL_ReceiverProcess,
                                TL_RecipientID = fileFound.TL_RecipientID,
                                TL_SenderID = fileFound.TL_SenderID,
                                TL_Subject = fileFound.TL_Subject,
                                TL_AppCode = fileFound.TL_AppCode,
                                TL_ToAddress = fileFound.TL_ToAddress
                            };
                            uow.TaskLists.Add(tl);
                            uow.Complete();
                        }
                        catch (Exception ex)
                        {
                            using (AppLogging log = new AppLogging(Globals.ConnString.ConnString))
                            {
                                log.AddLog(new AppLog
                                {

                                    GL_AppName = Assembly.GetExecutingAssembly().GetName().Name,
                                    GL_ServerName = Environment.MachineName,
                                    GL_Date = DateTime.Now,
                                    GL_ProcName = m.Name,
                                    GL_MessageType = "Error",
                                    GL_MessageLevel = 2,
                                    GL_MessageDetail = "Unabe to rename file " + file.Name + Environment.NewLine +
                                                        "Error: " + ex.GetType().Name + " :" + ex.Message
                                });
                            }
                        }


                    }
                    else
                    {
                        IdentifyFile iFile = new IdentifyFile(Globals.ConnString.ConnString);
                        var apiLog = iFile.IdFile(file.FullName);
                        if (apiLog == null)
                        {
                            using (AppLogging log = new AppLogging(Globals.ConnString.ConnString))
                            {
                                log.AddLog(new AppLog
                                {

                                    GL_AppName = Assembly.GetExecutingAssembly().GetName().Name,
                                    GL_ServerName = Environment.MachineName.ToString(),
                                    GL_Date = DateTime.Now,
                                    GL_ProcName = m.Name,
                                    GL_MessageType = "Error",
                                    GL_MessageLevel = 1,
                                    GL_MessageDetail = "File is Unable to be Identified in the ReProcess queue"
                                });

                            }

                        }
                        else
                        {
                            File.Move(file.FullName, Path.Combine(path, file.Name));
                        }


                    }


                }

            }
            _mainTimer.Start();

        }

        private void ProcessQueues()
        {
            MethodBase m = MethodBase.GetCurrentMethod();
            using (IHeartBeatManager heartBeat = new HeartBeatManager(Globals.ConnString.ConnString))
            {

                heartBeat.ExeName = Assembly.GetExecutingAssembly().GetName().Name;
                heartBeat.ExePath = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData),
                    System.Reflection.Assembly.GetExecutingAssembly().GetName().Name);

                heartBeat.RegisterHeartBeat("CTC", m.Name, m.GetParameters());
            }
            using (AppLogging log = new AppLogging(Globals.ConnString.ConnString))
            {

                log.AddLog(new AppLog
                {

                    GL_AppName = Assembly.GetExecutingAssembly().GetName().Name,
                    GL_ServerName = Environment.MachineName.ToString(),
                    GL_Date = DateTime.Now,
                    GL_ProcName = m.Name,
                    GL_MessageType = "Information",
                    GL_MessageLevel = 1,
                    GL_MessageDetail = m.Name + " started"
                });

            }
            _mainTimer.Stop();
            string primaryPath = string.Empty;
            string archivePath = string.Empty;
            string pickupPath = string.Empty;
            string outboundPath = string.Empty;
            string tempPath = string.Empty;

            if (tslMode.Text == "Production")
            {
                primaryPath = Globals.PrimaryLocation;
                archivePath = Globals.ArchiveLocation;
                pickupPath = Globals.PickupLocation;
                tempPath = Globals.TempLocation;
                outboundPath = Globals.OutboundLocation;
            }
            else
            {
                primaryPath = Globals.TestPrimaryLocation;
                archivePath = Globals.TestArchiveLocation;
                pickupPath = Globals.TestPickupLocation;
                tempPath = Globals.TestTempLocation;
                outboundPath = Globals.TestOutboundLocation;
            }

            foreach (Control cb in gbOperations.Controls)
            {
                if (cb is CheckBox)
                {
                    if (((CheckBox)cb).Checked)
                    {
                        switch (cb.Name)
                        {
                            case "cbEmail":
                                ProcessEmail(primaryPath, archivePath, tempPath, outboundPath);
                                break;
                            case "cbFtp":
                                ProcessFTP(primaryPath, archivePath, tempPath, outboundPath);
                                break;
                            case "cbEadaptor":
                                ProcessEadaptor(primaryPath, archivePath, pickupPath);
                                break;
                            case "cbFolder":
                                ProcessFolder(primaryPath, archivePath, outboundPath);
                                break;

                        }
                    }
                }
            }
            _mainTimer.Start();
        }

        // Processes to create a list of Profiles that need to be checked
        // P = Pickup
        // E = Email
        // C = CTC adaptor
        // F = FTP
        // Direction
        // R = Receive
        // S = Send
        // Active 
        // Y = Is Active
        // N = Not Active

        private void ProcessFolder(string primaryPath, string arcPath, string outboundPath)
        {
            MethodBase m = MethodBase.GetCurrentMethod();
            using (IHeartBeatManager heartBeat = new HeartBeatManager(Globals.ConnString.ConnString))
            {

                heartBeat.ExeName = Assembly.GetExecutingAssembly().GetName().Name;
                heartBeat.ExePath = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData),
                    System.Reflection.Assembly.GetExecutingAssembly().GetName().Name);

                heartBeat.RegisterHeartBeat("CTC", m.Name, m.GetParameters());
            }
            ProfileListBuilder pBuilder = new ProfileListBuilder(Globals.ConnString.ConnString);
            List<vw_CustomerProfile> profileList = pBuilder.BuildList("P", "R", "Y");
            //List<vw_CustomerProfile> sendList = pBuilder.BuildList("E", "S", "Y");
            if (GetLogLevel() == 2)
            {
                using (AppLogging log = new AppLogging(Globals.ConnString.ConnString))
                {

                    log.AddLog(new AppLog
                    {

                        GL_AppName = Assembly.GetExecutingAssembly().GetName().Name,
                        GL_ServerName = Environment.MachineName.ToString(),
                        GL_Date = DateTime.Now,
                        GL_ProcName = m.Name,
                        GL_MessageType = "Information",
                        GL_MessageLevel = 1,
                        GL_MessageDetail = m.Name + " started"
                    });

                }
            }

            eventStopWatch = new Stopwatch();
            if (profileList == null)
            {
                return;
            }
            foreach (vw_CustomerProfile p in profileList)
            {
                if (eventStopWatch == null)
                {
                    eventStopWatch = new Stopwatch();
                }
                eventStopWatch.Reset();
                eventStopWatch.Start();
                DirectoryInfo dir = new DirectoryInfo(p.P_PATH);
                if (p.P_MSGTYPE.Trim() == "PDF2XML")
                {
                    ProcessPDF2XML(p, primaryPath, arcPath);
                }

                
                    if (Directory.Exists(p.P_PATH))
                    {
                        int fCount = 0;
                        foreach (var infile in dir.GetFiles())
                        {
                            try
                            {
                                if (CheckIfStopped())
                                {
                                    return;
                                }
                                else
                                {
                                    string archiveName;
                                    using (ArcOps arcOps = new NodeResources.ArcOps())
                                    {
                                        archiveName = arcOps.ArchiveFile(Globals.ArchiveLocation, infile.FullName, Globals.AppLogPath);
                                    }

                                    if(p.P_MSGTYPE == "Forward")
                                {
                                    FileOps fi = new FileOps
                                    {
                                        OriginalFileName = infile.FullName,
                                        ProcessFunction = "Forward",
                                        OutboundPath = outboundPath,
                                        ReceivedFolder = p.P_PATH,
                                        RecipientId = p.P_RECIPIENTID,
                                        SenderId = p.P_SENDERID,
                                        ProfileID = p.P_OUTBOUND,
                                        ArchiveName = archiveName.Contains("Exception") ? string.Empty : archiveName
                                    };
                                    if (fi.AddOutboundFile())
                                    {
                                        fCount++;
                                    }
                                }
                                else
                                {
                                    FileOps fi = new FileOps
                                    {
                                        OriginalFileName = infile.FullName,
                                        ProcessFunction = "Pickup",
                                        PrimaryPath = primaryPath,
                                        ReceivedFolder = p.P_PATH,
                                        RecipientId = p.P_RECIPIENTID,
                                        SenderId = p.P_SENDERID,
                                        ProfileID = p.P_ID,
                                        ArchiveName = archiveName.Contains("Exception") ? string.Empty : archiveName
                                    };
                                    if (fi.AddFile())
                                    {
                                        fCount++;
                                    }
                                }
                                    
                                }
                                using (AppLogging log = new AppLogging(Globals.ConnString.ConnString))
                                {

                                    log.AddLog(new AppLog
                                    {

                                        GL_AppName = Assembly.GetExecutingAssembly().GetName().Name,
                                        GL_ServerName = Environment.MachineName.ToString(),
                                        GL_Date = DateTime.Now,
                                        GL_ProcName = m.Name,
                                        GL_MessageType = "Information",
                                        GL_MessageLevel = 1,
                                        GL_MessageDetail = "Processing File " + infile + ". " + eventStopWatch.Elapsed.TotalMinutes + " Minutes"
                                    });

                                }
                            }
                            catch (DbEntityValidationException ex)
                            {
                                using (AppLogging log = new AppLogging(Globals.ConnString.ConnString))
                                {

                                    log.AddLog(new AppLog
                                    {

                                        GL_AppName = Assembly.GetExecutingAssembly().GetName().Name,
                                        GL_ServerName = Environment.MachineName.ToString(),
                                        GL_Date = DateTime.Now,
                                        GL_ProcName = m.Name,
                                        GL_MessageType = "Exception",
                                        GL_MessageLevel = 1,
                                        GL_MessageDetail = "Error Processing " + p.P_DESCRIPTION + " File=" + infile.FullName + ". Error: " + ex.GetType().Name + " :" + ex.Message
                                    });

                                }

                            }
                            catch (Exception ex)
                            {
                                using (AppLogging log = new AppLogging(Globals.ConnString.ConnString))
                                {

                                    log.AddLog(new AppLog
                                    {

                                        GL_AppName = Assembly.GetExecutingAssembly().GetName().Name,
                                        GL_ServerName = Environment.MachineName.ToString(),
                                        GL_Date = DateTime.Now,
                                        GL_ProcName = m.Name,
                                        GL_MessageType = "Exception",
                                        GL_MessageLevel = 1,
                                        GL_MessageDetail = "Error Processing " + p.P_DESCRIPTION + " File=" + infile.FullName + ". Error: " + ex.GetType().Name + " :" + ex.Message
                                    });

                                }

                            }
                        }
                        eventStopWatch.Stop();
                        var eventTime = eventStopWatch.Elapsed.TotalSeconds;
                        if (GetLogLevel() == 2)
                        {
                            using (AppLogging log = new AppLogging(Globals.ConnString.ConnString))
                            {

                                log.AddLog(new AppLog
                                {

                                    GL_AppName = Assembly.GetExecutingAssembly().GetName().Name,
                                    GL_ServerName = Environment.MachineName.ToString(),
                                    GL_Date = DateTime.Now,
                                    GL_ProcName = m.Name,
                                    GL_MessageType = "Information",
                                    GL_MessageLevel = 1,
                                    GL_MessageDetail = fCount + " Files processed in " + eventTime + " Seconds"
                                });
                            }
                        }


                    }
                


                

            }

            //if (sendList == null)
            //{
            //    return;
            //}
            //foreach (vw_CustomerProfile p in sendList)
            //{
            //    if (eventStopWatch == null)
            //    {
            //        eventStopWatch = new Stopwatch();
            //    }
            //    eventStopWatch.Reset();
            //    eventStopWatch.Start();
            //    DirectoryInfo dir = new DirectoryInfo(p.C_PATH);
                
            //    if (Directory.Exists(p.C_PATH))
            //    {
            //        int fCount = 0;

            //        if(p.C_CODE == "TMC")
            //        {
            //            foreach (var infile in dir.GetFiles("*.txt"))
            //            {
            //                try
            //                {
            //                    string subject = Path.GetFileNameWithoutExtension(infile.Name).Split('-')[1];
            //                    SendFile(infile, p, subject.Replace("_", "/"));
            //                    using (AppLogging log = new AppLogging(Globals.ConnString.ConnString))
            //                    {

            //                        log.AddLog(new AppLog
            //                        {

            //                            GL_AppName = Assembly.GetExecutingAssembly().GetName().Name,
            //                            GL_ServerName = Environment.MachineName.ToString(),
            //                            GL_Date = DateTime.Now,
            //                            GL_ProcName = m.Name,
            //                            GL_MessageType = "Information",
            //                            GL_MessageLevel = 1,
            //                            GL_MessageDetail = "Processing File " + infile + ". " + eventStopWatch.Elapsed.TotalMinutes + " Minutes"
            //                        });

            //                    }
            //                }
            //                catch (DbEntityValidationException ex)
            //                {
            //                    using (AppLogging log = new AppLogging(Globals.ConnString.ConnString))
            //                    {

            //                        log.AddLog(new AppLog
            //                        {

            //                            GL_AppName = Assembly.GetExecutingAssembly().GetName().Name,
            //                            GL_ServerName = Environment.MachineName.ToString(),
            //                            GL_Date = DateTime.Now,
            //                            GL_ProcName = m.Name,
            //                            GL_MessageType = "Exception",
            //                            GL_MessageLevel = 1,
            //                            GL_MessageDetail = "Error Processing " + p.P_DESCRIPTION + " File=" + infile.FullName + ". Error: " + ex.GetType().Name + " :" + ex.Message
            //                        });

            //                    }

            //                }
            //                catch (Exception ex)
            //                {
            //                    using (AppLogging log = new AppLogging(Globals.ConnString.ConnString))
            //                    {

            //                        log.AddLog(new AppLog
            //                        {

            //                            GL_AppName = Assembly.GetExecutingAssembly().GetName().Name,
            //                            GL_ServerName = Environment.MachineName.ToString(),
            //                            GL_Date = DateTime.Now,
            //                            GL_ProcName = m.Name,
            //                            GL_MessageType = "Exception",
            //                            GL_MessageLevel = 1,
            //                            GL_MessageDetail = "Error Processing " + p.P_DESCRIPTION + " File=" + infile.FullName + ". Error: " + ex.GetType().Name + " :" + ex.Message
            //                        });

            //                    }

            //                }
            //            }
            //        }
                    
            //        eventStopWatch.Stop();
            //        var eventTime = eventStopWatch.Elapsed.TotalSeconds;
            //        if (GetLogLevel() == 2)
            //        {
            //            using (AppLogging log = new AppLogging(Globals.ConnString.ConnString))
            //            {

            //                log.AddLog(new AppLog
            //                {

            //                    GL_AppName = Assembly.GetExecutingAssembly().GetName().Name,
            //                    GL_ServerName = Environment.MachineName.ToString(),
            //                    GL_Date = DateTime.Now,
            //                    GL_ProcName = m.Name,
            //                    GL_MessageType = "Information",
            //                    GL_MessageLevel = 1,
            //                    GL_MessageDetail = fCount + " Files processed in " + eventTime + " Seconds"
            //                });
            //            }
            //        }


            //    }

            //}


        }

        public bool SendFile(FileInfo fileToSend, NodeData.Models.Ivw_CustomerProfile profile, string subject)
        {
            using (MailModule mailModule = new MailModule(Globals.MailServerSettings))
            {
                mailModule.SendMsg(fileToSend.FullName, profile.P_EMAILADDRESS, subject, "File attached");

            }

            return true;
        }

        private void ProcessPDF2XML(vw_CustomerProfile profile, string primaryPath, string arcPath)
        {
            MethodBase m = MethodBase.GetCurrentMethod();
            using (IHeartBeatManager heartBeat = new HeartBeatManager(Globals.ConnString.ConnString))
            {

                heartBeat.ExeName = Assembly.GetExecutingAssembly().GetName().Name;
                heartBeat.ExePath = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData),
                    System.Reflection.Assembly.GetExecutingAssembly().GetName().Name);

                heartBeat.RegisterHeartBeat("CTC", m.Name, m.GetParameters());
            }
            DirectoryInfo dir = new DirectoryInfo(profile.P_PATH);
            if (GetLogLevel() == 2)
            {
                using (AppLogging log = new AppLogging(Globals.ConnString.ConnString))
                {

                    log.AddLog(new AppLog
                    {

                        GL_AppName = Assembly.GetExecutingAssembly().GetName().Name,
                        GL_ServerName = Environment.MachineName.ToString(),
                        GL_Date = DateTime.Now,
                        GL_ProcName = m.Name,
                        GL_MessageType = "Information",
                        GL_MessageLevel = 1,
                        GL_MessageDetail = m.Name + " started"
                    });

                }

            }
            if (Directory.Exists(profile.P_PATH))
            {
                int fCount = 0;
                foreach (var infile in dir.GetFiles())
                {
                    if (CheckIfStopped())
                    {
                        return;
                    }
                    else
                    {
                        try
                        {
                            if (NodeFunctions.IsCommonXml(infile.FullName))
                            {

                                XDocument xCommon = XDocument.Load(infile.FullName);
                                var senderID = xCommon.Root.Element("IdentityMatrix").Element("CustomerId").Value;
                                var custProfile = GetPdfProfile(senderID);
                                if (custProfile == null)
                                {
                                    //TODO raise alert that a PDF File has been received and the Customer ID does not exist. 
                                    using (MailModule mm = new MailModule(Globals.MailServerSettings))
                                    {
                                        mm.SendMsg(infile.FullName, Globals.AlertsTo, "PDF2XML File found but no profile exists for " + senderID,
                                        "A PDF2XML file has been received but were unable to find the Associated Customer." + Environment.NewLine +
                                        "Please ensure that the Customer the best matches the SENDER code of " + senderID + " has been created in CTC Node." + Environment.NewLine +
                                        "If unsure of the Customer please contact ACS and ask for the Customer : Office  + 61-2-9699 6677 " + Environment.NewLine );

                                    };
                                    File.Delete(infile.FullName);
                                    return;
                                }

                                string archiveName;
                                using (ArcOps arcOps = new NodeResources.ArcOps())
                                {
                                    archiveName = arcOps.ArchiveFile(Globals.ArchiveLocation, infile.FullName, Globals.AppLogPath);
                                }
                                FileOps fi = new FileOps
                                {
                                    OriginalFileName = infile.FullName,
                                    ProcessFunction = "PDF2XML",
                                    PrimaryPath = primaryPath,
                                    ReceivedFolder = profile.P_PATH,
                                    RecipientId = custProfile.P_RECIPIENTID,
                                    SenderId = custProfile.P_SENDERID,
                                    ProfileID = custProfile.P_ID,
                                    ArchiveName = archiveName.Contains("Exception") ? string.Empty : archiveName
                                };
                                if (fi.AddFile())
                                {
                                    fCount++;
                                }


                            }

                        }

                        catch (Exception ex)
                        {
                            using (AppLogging log = new AppLogging(Globals.ConnString.ConnString))
                            {

                                log.AddLog(new AppLog
                                {

                                    GL_AppName = Assembly.GetExecutingAssembly().GetName().Name,
                                    GL_ServerName = Environment.MachineName.ToString(),
                                    GL_Date = DateTime.Now,
                                    GL_ProcName = m.Name,
                                    GL_MessageType = "Error",
                                    GL_MessageLevel = 1,
                                    GL_MessageDetail = "Error:" + ex.GetType().Name + " :" + ex.Message
                                });

                            }


                        }
                    }

                }
                var eventTime = eventStopWatch.Elapsed.TotalSeconds;
                if (GetLogLevel() == 2)
                {
                    using (AppLogging log = new AppLogging(Globals.ConnString.ConnString))
                    {

                        log.AddLog(new AppLog
                        {

                            GL_AppName = Assembly.GetExecutingAssembly().GetName().Name,
                            GL_ServerName = Environment.MachineName.ToString(),
                            GL_Date = DateTime.Now,
                            GL_ProcName = m.Name,
                            GL_MessageType = "Information",
                            GL_MessageLevel = 1,
                            GL_MessageDetail = fCount + " Files processed in " + eventTime + " Seconds"
                        });

                    }
                }


            }


        }

        private Profile GetPdfProfile(string senderID)
        {
            using (IUnitOfWork uow = new UnitOfWork(new NodeDataContext(Globals.ConnString.ConnString)))
            {
                var profile = uow.Profiles.Find(x => x.P_MSGTYPE.StartsWith("PDF2XML") && x.P_SENDERID == senderID).FirstOrDefault();
                if (profile == null)
                {
                    return null;
                }
                return profile;
            }
        }

        private void ProcessEadaptor(string primaryPath, string arcPath, string pickupPath)
        {
            MethodBase m = MethodBase.GetCurrentMethod();
            using (IHeartBeatManager heartBeat = new HeartBeatManager(Globals.ConnString.ConnString))
            {

                heartBeat.ExeName = Assembly.GetExecutingAssembly().GetName().Name;
                heartBeat.ExePath = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData),
                    System.Reflection.Assembly.GetExecutingAssembly().GetName().Name);

                heartBeat.RegisterHeartBeat("CTC", m.Name, m.GetParameters());
            }
            if (GetLogLevel() == 2)
            {
                using (AppLogging log = new AppLogging(Globals.ConnString.ConnString))
                {

                    log.AddLog(new AppLog
                    {

                        GL_AppName = Assembly.GetExecutingAssembly().GetName().Name,
                        GL_ServerName = Environment.MachineName.ToString(),
                        GL_Date = DateTime.Now,
                        GL_ProcName = m.Name,
                        GL_MessageType = "Information",
                        GL_MessageLevel = 1,
                        GL_MessageDetail = m.Name + " Started"
                    });

                }
            }

            DirectoryInfo dir = new DirectoryInfo(pickupPath);
            using (IUnitOfWork uow = new UnitOfWork(new NodeDataContext(Globals.ConnString.ConnString)))
            {
                if (eventStopWatch == null)
                {
                    eventStopWatch = new Stopwatch();
                }
                eventStopWatch.Reset();
                eventStopWatch.Start();
                int fCount = 0;
                foreach (FileInfo infile in dir.GetFiles())
                {
                    try
                    {

                        string archiveName;
                        using (ArcOps arcOps = new NodeResources.ArcOps())
                        {
                            archiveName = arcOps.ArchiveFile(Globals.ArchiveLocation, infile.FullName, Globals.AppLogPath);
                        }
                        var fileFound = uow.ApiLogs.Find(x => x.F_FILENAME == infile.Name).FirstOrDefault();
                        if (fileFound != null)
                        {
                            FileOps fi = new FileOps
                            {
                                OriginalFileName = infile.FullName,
                                ProcessFunction = "CTCAdaptor",
                                PrimaryPath = primaryPath,
                                ReceivedFolder = pickupPath,
                                SenderId = fileFound.F_SENDERID,
                                RecipientId = fileFound.F_CLIENTID,
                                Subject = fileFound.F_SUBJECT,
                                AppCode = fileFound.F_APPCODE,
                                CWFile = fileFound.F_CWFILENAME,
                                ArchiveName = archiveName
                            };
                            if (fi.AddFile())
                            {
                                fCount++;
                                //   _context.ApiLog.Remove(fileFound);
                                //   _context.SaveChanges();

                            }

                        }
                        else
                        {
                            IdentifyFile id = new IdentifyFile(Globals.ConnString.ConnString);
                            var apiLog = id.IdFile(infile.FullName);


                        }
                    }
                    catch (Exception ex)
                    {
                        using (MailModule mail = new MailModule(Globals.MailServerSettings))
                        {
                            mail.SendMsg(infile.FullName, Globals.AlertsTo, "Error Found Processing Inbound File", "Error was " + ex.Message + ": " + ex.InnerException);
                            infile.Delete();
                        }
                        using (AppLogging log = new AppLogging(Globals.ConnString.ConnString))
                        {

                            log.AddLog(new AppLog
                            {

                                GL_AppName = Assembly.GetExecutingAssembly().GetName().Name,
                                GL_ServerName = Environment.MachineName.ToString(),
                                GL_Date = DateTime.Now,
                                GL_ProcName = m.Name,
                                GL_MessageType = "Error",
                                GL_MessageLevel = 1,
                                GL_MessageDetail = "Error Processing CTC Adaptor File :" + infile.Name + Environment.NewLine +
                                                   "Error: " + ex.GetType().Name + " :" + ex.Message
                            });

                        }


                    }

                }
                eventStopWatch.Stop();
                var eventTime = eventStopWatch.Elapsed.TotalSeconds;

                if (GetLogLevel() == 2)
                {
                    using (AppLogging log = new AppLogging(Globals.ConnString.ConnString))
                    {

                        log.AddLog(new AppLog
                        {

                            GL_AppName = Assembly.GetExecutingAssembly().GetName().Name,
                            GL_ServerName = Environment.MachineName.ToString(),
                            GL_Date = DateTime.Now,
                            GL_ProcName = m.Name,
                            GL_MessageType = "Information",
                            GL_MessageLevel = 1,
                            GL_MessageDetail = fCount + " Files processed in " + eventTime + " Seconds"
                        });

                    }
                }


            }



        }

        private void ProcessFTP(string primaryPath, string arcPath, string tempPath, string outboundPath)
        {
            MethodBase m = MethodBase.GetCurrentMethod();
            using (IHeartBeatManager heartBeat = new HeartBeatManager(Globals.ConnString.ConnString))
            {

                heartBeat.ExeName = Assembly.GetExecutingAssembly().GetName().Name;
                heartBeat.ExePath = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData),
                    System.Reflection.Assembly.GetExecutingAssembly().GetName().Name);

                heartBeat.RegisterHeartBeat("CTC", m.Name, m.GetParameters());
            }
            if (GetLogLevel() == 2)
            {
                using (AppLogging log = new AppLogging(Globals.ConnString.ConnString))
                {

                    log.AddLog(new AppLog
                    {

                        GL_AppName = Assembly.GetExecutingAssembly().GetName().Name,
                        GL_ServerName = Environment.MachineName.ToString(),
                        GL_Date = DateTime.Now,
                        GL_ProcName = m.Name,
                        GL_MessageType = "Information",
                        GL_MessageLevel = 1,
                        GL_MessageDetail = m.Name + " Started."
                    });

                }
            }



            ProfileListBuilder pBuilder = new ProfileListBuilder(Globals.ConnString.ConnString);
            var profileList = pBuilder.BuildList("F", "R", "Y");
            foreach (vw_CustomerProfile p in profileList)
            {
                if (eventStopWatch == null)
                {
                    eventStopWatch = new Stopwatch();
                }
                eventStopWatch.Reset();
                eventStopWatch.Start();
                int fCount = 0;
                IFtpHelper ftpHelper = new FtpHelper(Globals.WinSCPLocation);

                ftpHelper.ReceivePath = Globals.TempLocation;
                if (GetLogLevel() == 2)
                {
                    using (AppLogging log = new AppLogging(Globals.ConnString.ConnString))
                    {

                        log.AddLog(new AppLog
                        {

                            GL_AppName = Assembly.GetExecutingAssembly().GetName().Name,
                            GL_ServerName = Environment.MachineName.ToString(),
                            GL_Date = DateTime.Now,
                            GL_ProcName = m.Name,
                            GL_MessageType = "Information",
                            GL_MessageLevel = 1,
                            GL_MessageDetail = "Starting FTP Queue (" + p.P_SERVER + ") :" + p.P_DESCRIPTION
                        });

                    }
                }


                if (p.P_SSL == "Y" ? true : false)
                {
                    ftpHelper.sshfingerPrint = p.P_EMAILADDRESS;
                    ftpHelper.PrivateKeyPath = p.P_SUBJECT;
                }
                var receivedList = ftpHelper.FTPReceive("|*/", p.P_SERVER, p.P_PORT, p.P_PATH, p.P_USERNAME, p.P_PASSWORD, p.P_SSL == "Y" ? true : false);
                if (receivedList == null)
                {
                    using (AppLogging log = new AppLogging(Globals.ConnString.ConnString))
                    {

                        log.AddLog(new AppLog
                        {

                            GL_AppName = Assembly.GetExecutingAssembly().GetName().Name,
                            GL_ServerName = Environment.MachineName.ToString(),
                            GL_Date = DateTime.Now,
                            GL_ProcName = m.Name,
                            GL_MessageType = "Warning",
                            GL_MessageLevel = 2,
                            GL_MessageDetail = "FTP Retrieve errors : Profile(" + p.P_DESCRIPTION + ")" + ftpHelper.ErrString
                        });

                    }
                    using (MailModule mail = new MailModule(Globals.MailServerSettings))
                    {
                        if (p.P_IGNOREMESSAGE != "Y")
                        {
                            mail.SendMsg(string.Empty, Globals.AlertsTo, "FTP Retrieve Errors found", ftpHelper.ErrString);
                        }

                    }
                    continue;
                }
                if (receivedList.Count == 0)
                {
                    if (string.IsNullOrEmpty(ftpHelper.ErrString))
                    {
                        continue;
                    }
                    using (MailModule mail = new MailModule(Globals.MailServerSettings))
                    {
                        if (p.P_IGNOREMESSAGE != "Y")
                        {
                            mail.SendMsg(string.Empty, Globals.AlertsTo, "FTP Retrieve Errors found", ftpHelper.ErrString);
                        }

                    }
                    continue;
                }
                else
                {
                    foreach (var file in receivedList)
                    {
                        if (ValidFile(file, p.P_FILETYPE))
                        {
                            string archiveName;
                            using (ArcOps arcOps = new NodeResources.ArcOps())
                            {
                                archiveName = arcOps.ArchiveFile(Globals.ArchiveLocation, file, Globals.AppLogPath);
                            }
                            if (p.P_MSGTYPE == "Forward")
                            {
                                FileOps fi = new FileOps
                                {
                                    OriginalFileName = file,
                                    ProcessFunction = "FTPIn",
                                    OutboundPath = outboundPath,
                                    ReceivedFolder = tempPath,
                                    SenderId = p.P_SENDERID,
                                    RecipientId = p.P_RECIPIENTID,
                                    ProfileID = p.P_OUTBOUND,
                                    ArchiveName = archiveName.Contains("Exception") ? string.Empty : archiveName

                                };
                                if (fi.AddOutboundFile())
                                {
                                    fCount++;
                                }
                            }
                            else
                            {

                                FileOps fi = new FileOps
                                {
                                    OriginalFileName = file,
                                    ProcessFunction = "FTPIn",
                                    PrimaryPath = primaryPath,
                                    ReceivedFolder = tempPath,
                                    SenderId = p.P_SENDERID,
                                    RecipientId = p.P_RECIPIENTID,
                                    ProfileID = p.P_ID,
                                    ArchiveName = archiveName.Contains("Exception") ? string.Empty : archiveName

                                };
                                if (fi.AddFile())
                                {
                                    fCount++;
                                }

                            }
                        }
                        else
                        {
                            File.Delete(file);
                        }

                    }
                }
                eventStopWatch.Stop();
                var eventTime = eventStopWatch.Elapsed.TotalSeconds;
                if (GetLogLevel() == 2)
                {
                    using (AppLogging log = new AppLogging(Globals.ConnString.ConnString))
                    {

                        log.AddLog(new AppLog
                        {

                            GL_AppName = Assembly.GetExecutingAssembly().GetName().Name,
                            GL_ServerName = Environment.MachineName.ToString(),
                            GL_Date = DateTime.Now,
                            GL_ProcName = m.Name,
                            GL_MessageType = "Information",
                            GL_MessageLevel = 1,
                            GL_MessageDetail = fCount + " Files processed in " + eventTime + " Seconds. FTP Server: " + p.P_SERVER
                        });
                    }
                }

            }
        }

        private bool ValidFile(string fileName, string p_FILETYPE)
        {
            if (p_FILETYPE.Trim() != null)
            {
                FileInfo fi = new FileInfo(fileName);
                if (p_FILETYPE.StartsWith("."))
                {
                    if (fi.Extension.ToUpper() == p_FILETYPE.Trim().ToUpper())
                    {
                        return true;
                    }
                }
                if (fi.Extension.ToUpper().Contains(p_FILETYPE.Trim().ToUpper()))
                {
                    return true;
                }
                return false;
            }
            else
            {
                return true;
            }
        }

        private void ProcessEmail(string primaryPath, string arcPath, string tempPat, string outboundPath)
        {
            MethodBase m = MethodBase.GetCurrentMethod();
            using (IHeartBeatManager heartBeat = new HeartBeatManager(Globals.ConnString.ConnString))
            {

                heartBeat.ExeName = Assembly.GetExecutingAssembly().GetName().Name;
                heartBeat.ExePath = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData),
                    System.Reflection.Assembly.GetExecutingAssembly().GetName().Name);

                heartBeat.RegisterHeartBeat("CTC", m.Name, m.GetParameters());
            }
            if (GetLogLevel() == 2)
            {
                using (AppLogging log = new AppLogging(Globals.ConnString.ConnString))
                {

                    log.AddLog(new AppLog
                    {

                        GL_AppName = Assembly.GetExecutingAssembly().GetName().Name,
                        GL_ServerName = Environment.MachineName.ToString(),
                        GL_Date = DateTime.Now,
                        GL_ProcName = m.Name,
                        GL_MessageType = "Information",
                        GL_MessageLevel = 1,
                        GL_MessageDetail = m.Name + " Starting"
                    });

                }
            }

            ProfileListBuilder pBuilder = new ProfileListBuilder(Globals.ConnString.ConnString);
            List<vw_CustomerProfile> profileList = pBuilder.BuildList("E", "R", "Y");
            if (profileList != null)
            {
                int fCount = 0;
                foreach (vw_CustomerProfile c in profileList)
                {
                    if (eventStopWatch == null)
                    {
                        eventStopWatch = new Stopwatch();
                    }
                    eventStopWatch.Reset();
                    eventStopWatch.Start();
                    try
                    {
                        IMailServerSettings settings = new MailServerSettings
                        {
                            Server = c.P_SERVER,
                            Password = c.P_PASSWORD,
                            UserName = c.P_USERNAME,
                            Port = int.Parse(c.P_PORT),
                            IsSecure = c.P_SSL == "Y" ? true : false
                        };
                        using (MailModule mail = new MailModule(settings, "", Globals.TempLocation))
                        {
                            mail.CheckMail();
                            List<IMsgSummary> msgList = mail.GetMail();
                            if (mail.ErrorsFound)
                            {
                                using (AppLogging log = new AppLogging(Globals.ConnString.ConnString))
                                {

                                    log.AddLog(new AppLog
                                    {

                                        GL_AppName = Assembly.GetExecutingAssembly().GetName().Name,
                                        GL_ServerName = Environment.MachineName.ToString(),
                                        GL_Date = DateTime.Now,
                                        GL_ProcName = m.Name,
                                        GL_MessageType = "Warning",
                                        GL_MessageLevel = 2,
                                        GL_MessageDetail = "Error processing Email:" + c.P_EMAILADDRESS + Environment.NewLine + mail.ErrMsg
                                    });

                                }
                                continue;
                            }
                            foreach (IMsgSummary msg in msgList)
                            {
                                if (msg.MsgFile.Count > 0)
                                {
                                    if (msg.MsgFile != null)
                                    {
                                        foreach (MsgAtt att in msg.MsgFile)
                                        {
                                            if ((ValidFile(att.AttFilename.FullName, c.P_FILETYPE)) && (ValidEmail(msg, c)))
                                            {
                                                string archiveName;
                                                try
                                                {
                                                    using (ArcOps arcOps = new NodeResources.ArcOps())
                                                    {
                                                        archiveName = arcOps.ArchiveFile(Globals.ArchiveLocation, att.AttFilename.FullName, Globals.AppLogPath);
                                                    }

                                                    if(c.P_MSGTYPE == "Forward")
                                                    {
                                                        FileOps fi = new FileOps
                                                        {
                                                            OriginalFileName = att.AttFilename.FullName,
                                                            ProcessFunction = "Email",
                                                            OutboundPath = outboundPath,
                                                            ReceivedFolder = string.Empty,
                                                            SenderId = c.P_SENDERID,
                                                            RecipientId = c.P_RECIPIENTID,
                                                            Subject = msg.MsgSubject.Truncate(100),
                                                            FromAddress = msg.MsgFrom != null ? msg.MsgFrom : "Email Address Missing",
                                                            ToAddress = c.P_EMAILADDRESS,
                                                            ProfileID = c.P_OUTBOUND,
                                                            ArchiveName = archiveName
                                                        };
                                                        if (fi.AddOutboundFile())
                                                        {

                                                            fCount++;
                                                        }
                                                        else
                                                        {
                                                            throw new Exception();
                                                        }
                                                    }
                                                    else
                                                    {
                                                        FileOps fi = new FileOps
                                                        {
                                                            OriginalFileName = att.AttFilename.FullName,
                                                            ProcessFunction = "Email",
                                                            PrimaryPath = primaryPath,
                                                            ReceivedFolder = string.Empty,
                                                            SenderId = c.P_SENDERID,
                                                            RecipientId = c.P_RECIPIENTID,
                                                            Subject = msg.MsgSubject.Truncate(100),
                                                            FromAddress = msg.MsgFrom != null ? msg.MsgFrom : "Email Address Missing",
                                                            ToAddress = c.P_EMAILADDRESS,
                                                            ProfileID = c.P_ID,
                                                            ArchiveName = archiveName
                                                        };
                                                        if (fi.AddFile())
                                                        {

                                                            fCount++;
                                                        }
                                                        else
                                                        {
                                                            throw new Exception();
                                                        }
                                                    }

                                                    
                                                }
                                                catch (Exception ex)
                                                {
                                                    using (AppLogging log = new AppLogging(Globals.ConnString.ConnString))
                                                    {

                                                        log.AddLog(new AppLog
                                                        {

                                                            GL_AppName = Assembly.GetExecutingAssembly().GetName().Name,
                                                            GL_ServerName = Environment.MachineName.ToString(),
                                                            GL_Date = DateTime.Now,
                                                            GL_ProcName = m.Name,
                                                            GL_MessageType = "Error",
                                                            GL_MessageLevel = 1,
                                                            GL_MessageDetail = "Error processing Email" + c.P_EMAILADDRESS + Environment.NewLine + "Error: " + ex.GetType().Name + " :" + ex.Message
                                                        });

                                                    }

                                                }


                                            }
                                            else
                                            {
                                                try
                                                {
                                                    System.GC.Collect();
                                                    System.GC.WaitForPendingFinalizers();
                                                    File.Delete(att.AttFilename.FullName);
                                                }
                                                catch (Exception ex)
                                                {
                                                    using (AppLogging log = new AppLogging(Globals.ConnString.ConnString))
                                                    {

                                                        log.AddLog(new AppLog
                                                        {

                                                            GL_AppName = Assembly.GetExecutingAssembly().GetName().Name,
                                                            GL_ServerName = Environment.MachineName.ToString(),
                                                            GL_Date = DateTime.Now,
                                                            GL_ProcName = m.Name,
                                                            GL_MessageType = "Error",
                                                            GL_MessageLevel = 1,
                                                            GL_MessageDetail = "Error processing Email" + c.P_EMAILADDRESS + Environment.NewLine + "Error: " + ex.GetType().Name + " :" + ex.Message
                                                        });

                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        using (AppLogging log = new AppLogging(Globals.ConnString.ConnString))
                        {

                            log.AddLog(new AppLog
                            {

                                GL_AppName = Assembly.GetExecutingAssembly().GetName().Name,
                                GL_ServerName = Environment.MachineName.ToString(),
                                GL_Date = DateTime.Now,
                                GL_ProcName = m.Name,
                                GL_MessageType = "Error",
                                GL_MessageLevel = 1,
                                GL_MessageDetail = "Error processing Email" + c.P_EMAILADDRESS + Environment.NewLine + "Error: " + ex.GetType().Name + " :" + ex.Message
                            });

                        }

                    }

                    eventStopWatch.Stop();
                    var eventTime = eventStopWatch.Elapsed.TotalSeconds;
                    if (GetLogLevel() == 2)
                    {
                        using (AppLogging log = new AppLogging(Globals.ConnString.ConnString))
                        {

                            log.AddLog(new AppLog
                            {

                                GL_AppName = Assembly.GetExecutingAssembly().GetName().Name,
                                GL_ServerName = Environment.MachineName.ToString(),
                                GL_Date = DateTime.Now,
                                GL_ProcName = m.Name,
                                GL_MessageType = "Information",
                                GL_MessageLevel = 1,
                                GL_MessageDetail = fCount + " Files processed in " + eventTime + "Seconds"
                            });

                        }
                    }

                }


            }

        }

        private bool ValidEmail(IMsgSummary msg, vw_CustomerProfile c)
        {
            bool emailValid = true;
            if (!string.IsNullOrEmpty(c.P_SENDEREMAIL))
            {
                emailValid = msg.MsgFrom.ToUpper().Contains(c.P_SENDEREMAIL.Trim().ToUpper()) ? true : false;
            }

            if (!string.IsNullOrEmpty(c.P_SUBJECT))
            {
                emailValid = msg.MsgSubject.ToUpper().Contains(c.P_SUBJECT.Trim().ToUpper()) ? true : false;
            }

            if (!string.IsNullOrEmpty(c.P_EMAILADDRESS))
            {
                emailValid = msg.MsgTo.ToUpper().Contains(c.P_EMAILADDRESS.Trim().ToUpper()) ? true : false;
            }
            return emailValid;
        }

        public bool CheckIfStopped()
        {
            if (btnStart.Text == "&Start")
            {
                return true;
            }
            else
            {
                return false;
            }

        }
        private void btnClose_Click(object sender, EventArgs e)
        {
            DialogResult dr = new DialogResult();

            this.Close();
        }

        private void exitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnStart_Click(object sender, EventArgs e)
        {
            if (btnStart.Text == "&Start")
            {
                btnStart.Text = "&Stop";
                _mainTimer.Enabled = true;
                _highTideTimer.Enabled = true;
                ProcessQueues();
                ReProcessQueue();
            }
            else
            if (btnStart.Text == "&Stop")
            {
                btnStart.Text = "&Start";
                _mainTimer.Stop();
                _highTideTimer.Stop();
                _highTideTimer.Enabled = false;
                _mainTimer.Enabled = false;
                using (AppLogging log = new AppLogging(Globals.ConnString.ConnString))
                {

                    log.AddLog(new AppLog
                    {

                        GL_AppName = Assembly.GetExecutingAssembly().GetName().Name,
                        GL_ServerName = Environment.MachineName.ToString(),
                        GL_Date = DateTime.Now,
                        GL_ProcName = "Timed Processes",
                        GL_MessageType = "Information",
                        GL_MessageLevel = 1,
                        GL_MessageDetail = "Timed Processes Stopped"
                    });

                }

            }
        }

        private void settingsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Settings settings = new Settings();
            settings.ShowDialog();
            LoadSettings();
        }

        private void MainForm_Load(object sender, EventArgs e)
        {
            tslMain.Width = this.Width / 2;
            tslSpacer.Width = this.Width / 2 - (tslCmbMode.Width + productionToolStripMenuItem.Width);
            LoadSettings();
            MethodBase m = MethodBase.GetCurrentMethod();
            using (IHeartBeatManager heartBeat = new HeartBeatManager(Globals.ConnString.ConnString))
            {

                heartBeat.ExeName = Assembly.GetExecutingAssembly().GetName().Name;
                heartBeat.ExePath = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData),
                    System.Reflection.Assembly.GetExecutingAssembly().GetName().Name);

                heartBeat.RegisterHeartBeat("CTC", "Starting", null);
            }
            tslMode.Text = "Production";
            CheckDb();
            string appLog = System.Reflection.Assembly.GetExecutingAssembly().GetName().Name + "-Log.XML";
            Globals.AppLogPath = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData),
                System.Reflection.Assembly.GetExecutingAssembly().GetName().Name, appLog);
            using (AppLogging log = new AppLogging(Globals.ConnString.ConnString))
            {

                log.AddLog(new AppLog
                {

                    GL_AppName = Assembly.GetExecutingAssembly().GetName().Name,
                    GL_ServerName = Environment.MachineName.ToString(),
                    GL_Date = DateTime.Now,
                    GL_ProcName = "Starting",
                    GL_MessageType = "Information",
                    GL_MessageLevel = 1,
                    GL_MessageDetail = "Applicaiton starting"
                });

            }


        }

        private int LogLevel()
        {
            int result = 0;
            foreach (RadioButton c in this.gbLogging.Controls)
            {
                if (c.Checked)
                {
                    result = int.Parse(c.Tag.ToString());
                    return result;
                }
            }

            return result;
        }

        private void CheckDb()
        {
            using (IUnitOfWork uow = new UnitOfWork(new NodeDataContext(Globals.ConnString.ConnString)))
            {
                try
                {
                    if (!uow.DBExists())
                    {

                    }
                }
                catch (Exception ex)
                {
                    MessageBox.Show("Error Connecting to the Database. Error was :" + ex.GetType().Name + Environment.NewLine + "Error details: " + ex.Message
                        + Environment.NewLine + "Check Database Settings");
                    Settings settings = new Settings();
                    settings.ShowDialog();
                }
            }

        }

        private void LoadSettings()
        {
            var appDataPath = Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData);

            var path = Path.Combine(appDataPath, System.Reflection.Assembly.GetExecutingAssembly().GetName().Name);
            if (!Directory.Exists(path))
            {
                Directory.CreateDirectory(path);
            }
            Globals.AppPath = path;
            Globals.AppConfig = System.Reflection.Assembly.GetExecutingAssembly().GetName().Name + ".XML";
            Globals.AppConfig = Path.Combine(path, Globals.AppConfig);
            Settings settings = new Settings();
            if (!File.Exists(Globals.AppConfig))
            {
                XDocument xmlConfig = new XDocument(
                            new XDeclaration("1.0", "UTF-8", "Yes"),
                            new XElement("InboundProcessor",
                            new XElement("Database"),
                            new XElement("Communications"),
                            new XElement("Config")));
                xmlConfig.Save(Globals.AppConfig);
                settings.ShowDialog();
                LoadSettings();
            }
            else
            {
                try
                {
                    XDocument doc = XDocument.Load(Globals.AppConfig);

                    XmlDocument xmlConfig = new XmlDocument();
                    xmlConfig.Load(Globals.AppConfig);

                    var node = doc.Root.Element("Database");

                    if (node == null)
                    {
                        MessageBox.Show("Error Loading Some of the System settings.", "System Settings missing", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                        settings.ShowDialog();
                        LoadSettings();
                    }
                    Globals.ConnString = new ConnectionManager(node.Element("ServerName").Value, node.Element("DatabaseName").Value,
                                                                node.Element("UserName").Value, node.Element("Password").Value);
                    node = doc.Root.Element("Communications");
                    if (node == null)
                    {
                        MessageBox.Show("Error Loading Some of the System settings.", "System Settings missing", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                        settings.ShowDialog();
                        LoadSettings();
                    }
                    Globals.MailServerSettings = new MailServerSettings
                    {
                        Server = node.Element("SMTPServer").Value,
                        UserName = node.Element("SMTPUser").Value,
                        Password = node.Element("SMTPPassword").Value,
                        IsSecure = bool.Parse(node.Element("SMTPSsl").Value),
                        Port = int.Parse(node.Element("SMTPPort").Value),
                        Email = node.Element("SMTPEmailFrom").Value
                    };
                    node = doc.Root.Element("Config");
                    if (node == null)
                    {
                        MessageBox.Show("Error Loading Some of the System settings.", "System Settings missing", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                        settings.ShowDialog();
                        LoadSettings();
                    }
                    Globals.WinSCPLocation = node.Element("WinSCPLocation") == null ? "" : node.Element("WinSCPLocation").Value;
                    Globals.PrimaryLocation = node.Element("PrimaryLocation") == null ? "" : node.Element("PrimaryLocation").Value;
                    Globals.TempLocation = node.Element("TemporaryLocation") == null ? "" : node.Element("TemporaryLocation").Value;
                    Globals.ArchiveLocation = node.Element("ArchiveLocation") == null ? "" : node.Element("ArchiveLocation").Value;
                    Globals.TestPrimaryLocation = node.Element("TestPrimaryLocation") == null ? "" : node.Element("TestPrimaryLocation").Value;
                    Globals.TestTempLocation = node.Element("TestTemporaryLocation") == null ? "" : node.Element("TestTemporaryLocation").Value;
                    Globals.TestArchiveLocation = node.Element("TestArchiveLocation") == null ? "" : node.Element("TestArchiveLocation").Value;
                    Globals.ReProcessLocation = node.Element("ReProcessLocation") == null ? "" : node.Element("ReProcessLocation").Value;
                    Globals.TestReProcessLocation = node.Element("TestReProcessLocation") == null ? "" : node.Element("TestReProcessLocation").Value;
                    Globals.PollTime = node.Element("PollTime") == null ? 0 : int.Parse(node.Element("PollTime").Value);
                    Globals.RefreshTime = node.Element("RefreshTime") == null ? 0 : int.Parse(node.Element("RefreshTime").Value);
                    Globals.AlertsTo = node.Element("AlertsTo") == null ? "" : node.Element("AlertsTo").Value;
                    Globals.PickupLocation = node.Element("PickupLocation") == null ? "" : node.Element("PickupLocation").Value;
                    Globals.TestPickupLocation = node.Element("TestPickupLocation") == null ? "" : node.Element("TestPickupLocation").Value;
                }
                catch (Exception)
                {
                    MessageBox.Show("Error Loading Some of the System settings.", "System Settings missing", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    settings.ShowDialog();
                    LoadSettings();
                }
                _refreshTimer.Interval = (int)TimeSpan.FromMinutes(Globals.RefreshTime).TotalMilliseconds;
                _refreshTimer.Start();
                _mainTimer.Interval = (int)TimeSpan.FromMinutes(Globals.PollTime).TotalMilliseconds;
            }
        }

        private void productionToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (testingToolStripMenuItem.Checked)
            {
                productionToolStripMenuItem.Checked = true;
                testingToolStripMenuItem.Checked = false;
                tslMode.Text = "Production";
            }
            else
            {
                productionToolStripMenuItem.Checked = false;
                testingToolStripMenuItem.Checked = true;
                tslMode.Text = "Testing";
            }
        }

        private void TestingToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (productionToolStripMenuItem.Checked)
            {
                testingToolStripMenuItem.Checked = true;
                productionToolStripMenuItem.Checked = false;
                tslMode.Text = "Testing";
            }
            else
            {
                testingToolStripMenuItem.Checked = false;
                productionToolStripMenuItem.Checked = true;
                tslMode.Text = "Production";
            }
        }

        private void MainForm_Resize(object sender, EventArgs e)
        {
            tslSpacer.Width = this.Width / 2 - (tslCmbMode.Width + productionToolStripMenuItem.Width);
        }

        private void bbClearLog_Click(object sender, EventArgs e)
        {
            try
            {
                File.Delete(Globals.AppLogPath);

                string cp = "Clearing Log";
                string de = "App Log Cleared";
                DateTime ti = DateTime.Now;
                string er = string.Empty;


            }
            catch (Exception ex)
            {
                string strEx = ex.GetType().Name + " Error: " + ex.Message + Environment.NewLine
                        + "---------------------------------------------" + Environment.NewLine +
                        ex.StackTrace;
                MailModule mm = new MailModule(Globals.MailServerSettings);
                mm.SendMsg("", "andy.duggan@ctcorp.com.au", "Error Clearing log", strEx);
            }

        }

        private void MainForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            MethodBase m = MethodBase.GetCurrentMethod();
            using (IHeartBeatManager heartBeat = new HeartBeatManager(Globals.ConnString.ConnString))
            {

                heartBeat.ExeName = Assembly.GetExecutingAssembly().GetName().Name;
                heartBeat.ExePath = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData),
                    System.Reflection.Assembly.GetExecutingAssembly().GetName().Name);

                heartBeat.RegisterHeartBeat("CTC", m.Name, m.GetParameters());
            }
        }

        private void grdLogging_CellFormatting(object sender, DataGridViewCellFormattingEventArgs e)
        {
            //DataGridViewRow dgRow = grdLogging.Rows(e.RowIndex);
            //DataGridViewImageCell cell = (DataGridViewImageCell)grdLogging.Rows[e.RowIndex].Cells[0];
            //if (e.RowIndex > -1 && e.ColumnIndex ==0)
            //{
            //    System.Drawing.Image imageForGrid = null;
            //    switch (grdLogging.Rows[e.RowIndex].Cells["colType"].Value)
            //    {
            //        case "Warning":
            //            imageForGrid = ilMain.Images["Warning"];
            //            break;
            //        case "Information":
            //            imageForGrid = ilMain.Images["Information"];
            //            break;

            //        case "Error":
            //            imageForGrid = ilMain.Images["Error"];
            //            break;
            //    }
            //    e.CellStyle.BackColor = e.CellStyle.BackColor;
            //    e.FormattingApplied = true;
            //    cell.Value = imageForGrid;

            //}



        }

        private void purgeLogsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            frmPurgeLogs purgeLogs = new frmPurgeLogs();
            purgeLogs.ShowDialog();
            DialogResult dr = purgeLogs.DialogResult;


        }



    }
}

