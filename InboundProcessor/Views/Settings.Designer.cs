﻿namespace InboundProcessor.Views
{
    partial class Settings
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnClose = new System.Windows.Forms.Button();
            this.tcSettings = new System.Windows.Forms.TabControl();
            this.tbSettings = new System.Windows.Forms.TabPage();
            this.button1 = new System.Windows.Forms.Button();
            this.edWinSCP = new System.Windows.Forms.TextBox();
            this.label27 = new System.Windows.Forms.Label();
            this.label23 = new System.Windows.Forms.Label();
            this.txtRefresh = new System.Windows.Forms.NumericUpDown();
            this.label24 = new System.Windows.Forms.Label();
            this.txtAlertsTo = new System.Windows.Forms.TextBox();
            this.label14 = new System.Windows.Forms.Label();
            this.label22 = new System.Windows.Forms.Label();
            this.txtPollTime = new System.Windows.Forms.NumericUpDown();
            this.label21 = new System.Windows.Forms.Label();
            this.tbDatabase = new System.Windows.Forms.TabPage();
            this.tableLayoutPanel2 = new System.Windows.Forms.TableLayoutPanel();
            this.label5 = new System.Windows.Forms.Label();
            this.txtConnectionResults = new System.Windows.Forms.TextBox();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.label1 = new System.Windows.Forms.Label();
            this.txtDatabaseServer = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.txtDatabaseName = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.txtUserName = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.txtPassword = new System.Windows.Forms.TextBox();
            this.btnTest = new System.Windows.Forms.Button();
            this.tbFileLocations = new System.Windows.Forms.TabPage();
            this.label20 = new System.Windows.Forms.Label();
            this.label19 = new System.Windows.Forms.Label();
            this.panel2 = new System.Windows.Forms.Panel();
            this.btnReProcessLocation = new System.Windows.Forms.Button();
            this.txtReProcessLoc = new System.Windows.Forms.TextBox();
            this.label28 = new System.Windows.Forms.Label();
            this.btnPickupLocation = new System.Windows.Forms.Button();
            this.txtPickupLocation = new System.Windows.Forms.TextBox();
            this.label26 = new System.Windows.Forms.Label();
            this.btnArchiveLocation = new System.Windows.Forms.Button();
            this.txtArchiveLocation = new System.Windows.Forms.TextBox();
            this.label16 = new System.Windows.Forms.Label();
            this.btnPrimaryLocation = new System.Windows.Forms.Button();
            this.txtPrimaryLocation = new System.Windows.Forms.TextBox();
            this.label17 = new System.Windows.Forms.Label();
            this.btnTempLocation = new System.Windows.Forms.Button();
            this.txtTempLocation = new System.Windows.Forms.TextBox();
            this.label18 = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.btnTestReProcessQueue = new System.Windows.Forms.Button();
            this.txtTestReProcessLocation = new System.Windows.Forms.TextBox();
            this.label29 = new System.Windows.Forms.Label();
            this.btnTestPickupLocation = new System.Windows.Forms.Button();
            this.txtTestPickupLocation = new System.Windows.Forms.TextBox();
            this.label25 = new System.Windows.Forms.Label();
            this.btnTestArchiveLocation = new System.Windows.Forms.Button();
            this.txtTestArchiveLocation = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.btnTestPrimaryLocation = new System.Windows.Forms.Button();
            this.txtTestPrimaryLocation = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.btnTestTempLocation = new System.Windows.Forms.Button();
            this.txtTestTempLocation = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.tbCommunications = new System.Windows.Forms.TabPage();
            this.cbSMTPSecure = new System.Windows.Forms.CheckBox();
            this.txtSmtpEmailAddress = new System.Windows.Forms.TextBox();
            this.label13 = new System.Windows.Forms.Label();
            this.txtSmtpPassword = new System.Windows.Forms.TextBox();
            this.label12 = new System.Windows.Forms.Label();
            this.txtSmtpUserName = new System.Windows.Forms.TextBox();
            this.label11 = new System.Windows.Forms.Label();
            this.txtSmtpPort = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.txtSmtpServer = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.btnSave = new System.Windows.Forms.Button();
            this.label30 = new System.Windows.Forms.Label();
            this.txtOutboundLocation = new System.Windows.Forms.TextBox();
            this.btnOutboundLocation = new System.Windows.Forms.Button();
            this.label31 = new System.Windows.Forms.Label();
            this.txtTestOutboundLocation = new System.Windows.Forms.TextBox();
            this.btnTestOutboundLocation = new System.Windows.Forms.Button();
            this.tcSettings.SuspendLayout();
            this.tbSettings.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtRefresh)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPollTime)).BeginInit();
            this.tbDatabase.SuspendLayout();
            this.tableLayoutPanel2.SuspendLayout();
            this.tableLayoutPanel1.SuspendLayout();
            this.tbFileLocations.SuspendLayout();
            this.panel2.SuspendLayout();
            this.panel1.SuspendLayout();
            this.tbCommunications.SuspendLayout();
            this.SuspendLayout();
            // 
            // btnClose
            // 
            this.btnClose.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnClose.Location = new System.Drawing.Point(693, 627);
            this.btnClose.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(100, 28);
            this.btnClose.TabIndex = 0;
            this.btnClose.Text = "&Close";
            this.btnClose.UseVisualStyleBackColor = true;
            this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // tcSettings
            // 
            this.tcSettings.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tcSettings.Controls.Add(this.tbSettings);
            this.tcSettings.Controls.Add(this.tbDatabase);
            this.tcSettings.Controls.Add(this.tbFileLocations);
            this.tcSettings.Controls.Add(this.tbCommunications);
            this.tcSettings.Location = new System.Drawing.Point(16, 34);
            this.tcSettings.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.tcSettings.Name = "tcSettings";
            this.tcSettings.SelectedIndex = 0;
            this.tcSettings.Size = new System.Drawing.Size(777, 585);
            this.tcSettings.TabIndex = 1;
            // 
            // tbSettings
            // 
            this.tbSettings.Controls.Add(this.button1);
            this.tbSettings.Controls.Add(this.edWinSCP);
            this.tbSettings.Controls.Add(this.label27);
            this.tbSettings.Controls.Add(this.label23);
            this.tbSettings.Controls.Add(this.txtRefresh);
            this.tbSettings.Controls.Add(this.label24);
            this.tbSettings.Controls.Add(this.txtAlertsTo);
            this.tbSettings.Controls.Add(this.label14);
            this.tbSettings.Controls.Add(this.label22);
            this.tbSettings.Controls.Add(this.txtPollTime);
            this.tbSettings.Controls.Add(this.label21);
            this.tbSettings.Location = new System.Drawing.Point(4, 25);
            this.tbSettings.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.tbSettings.Name = "tbSettings";
            this.tbSettings.Padding = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.tbSettings.Size = new System.Drawing.Size(733, 436);
            this.tbSettings.TabIndex = 3;
            this.tbSettings.Text = "Settings";
            this.tbSettings.UseVisualStyleBackColor = true;
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(648, 94);
            this.button1.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(33, 25);
            this.button1.TabIndex = 20;
            this.button1.Text = "...";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // edWinSCP
            // 
            this.edWinSCP.Location = new System.Drawing.Point(208, 94);
            this.edWinSCP.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.edWinSCP.Name = "edWinSCP";
            this.edWinSCP.Size = new System.Drawing.Size(439, 22);
            this.edWinSCP.TabIndex = 19;
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.Location = new System.Drawing.Point(24, 97);
            this.label27.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(164, 17);
            this.label27.TabIndex = 18;
            this.label27.Text = "Location of WinSCP.EXE";
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Location = new System.Drawing.Point(632, 27);
            this.label23.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(57, 17);
            this.label23.TabIndex = 17;
            this.label23.Text = "Minutes";
            // 
            // txtRefresh
            // 
            this.txtRefresh.Location = new System.Drawing.Point(563, 25);
            this.txtRefresh.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.txtRefresh.Name = "txtRefresh";
            this.txtRefresh.Size = new System.Drawing.Size(60, 22);
            this.txtRefresh.TabIndex = 16;
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Location = new System.Drawing.Point(363, 27);
            this.label24.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(191, 17);
            this.label24.TabIndex = 15;
            this.label24.Text = "Default Screen Refresh Time";
            // 
            // txtAlertsTo
            // 
            this.txtAlertsTo.Location = new System.Drawing.Point(185, 57);
            this.txtAlertsTo.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.txtAlertsTo.Name = "txtAlertsTo";
            this.txtAlertsTo.Size = new System.Drawing.Size(481, 22);
            this.txtAlertsTo.TabIndex = 14;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(24, 60);
            this.label14.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(126, 17);
            this.label14.TabIndex = 13;
            this.label14.Text = "Global Alerts email";
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Location = new System.Drawing.Point(253, 27);
            this.label22.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(57, 17);
            this.label22.TabIndex = 2;
            this.label22.Text = "Minutes";
            // 
            // txtPollTime
            // 
            this.txtPollTime.Location = new System.Drawing.Point(185, 25);
            this.txtPollTime.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.txtPollTime.Name = "txtPollTime";
            this.txtPollTime.Size = new System.Drawing.Size(60, 22);
            this.txtPollTime.TabIndex = 1;
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Location = new System.Drawing.Point(25, 27);
            this.label21.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(134, 17);
            this.label21.TabIndex = 0;
            this.label21.Text = "Default Polling Time";
            // 
            // tbDatabase
            // 
            this.tbDatabase.Controls.Add(this.tableLayoutPanel2);
            this.tbDatabase.Controls.Add(this.tableLayoutPanel1);
            this.tbDatabase.Location = new System.Drawing.Point(4, 25);
            this.tbDatabase.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.tbDatabase.Name = "tbDatabase";
            this.tbDatabase.Padding = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.tbDatabase.Size = new System.Drawing.Size(733, 436);
            this.tbDatabase.TabIndex = 0;
            this.tbDatabase.Text = "Database Connections";
            this.tbDatabase.UseVisualStyleBackColor = true;
            // 
            // tableLayoutPanel2
            // 
            this.tableLayoutPanel2.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.tableLayoutPanel2.ColumnCount = 1;
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel2.Controls.Add(this.label5, 0, 0);
            this.tableLayoutPanel2.Controls.Add(this.txtConnectionResults, 0, 1);
            this.tableLayoutPanel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel2.Location = new System.Drawing.Point(4, 138);
            this.tableLayoutPanel2.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.tableLayoutPanel2.Name = "tableLayoutPanel2";
            this.tableLayoutPanel2.RowCount = 2;
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel2.Size = new System.Drawing.Size(725, 294);
            this.tableLayoutPanel2.TabIndex = 1;
            this.tableLayoutPanel2.Paint += new System.Windows.Forms.PaintEventHandler(this.tableLayoutPanel2_Paint);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(4, 0);
            this.label5.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(99, 17);
            this.label5.TabIndex = 0;
            this.label5.Text = "Test Results";
            // 
            // txtConnectionResults
            // 
            this.txtConnectionResults.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtConnectionResults.Location = new System.Drawing.Point(4, 131);
            this.txtConnectionResults.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.txtConnectionResults.Multiline = true;
            this.txtConnectionResults.Name = "txtConnectionResults";
            this.txtConnectionResults.Size = new System.Drawing.Size(717, 159);
            this.txtConnectionResults.TabIndex = 1;
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.tableLayoutPanel1.ColumnCount = 3;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 137F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 357F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tableLayoutPanel1.Controls.Add(this.label1, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.txtDatabaseServer, 1, 0);
            this.tableLayoutPanel1.Controls.Add(this.label2, 0, 1);
            this.tableLayoutPanel1.Controls.Add(this.txtDatabaseName, 1, 1);
            this.tableLayoutPanel1.Controls.Add(this.label3, 0, 2);
            this.tableLayoutPanel1.Controls.Add(this.txtUserName, 1, 2);
            this.tableLayoutPanel1.Controls.Add(this.label4, 0, 3);
            this.tableLayoutPanel1.Controls.Add(this.txtPassword, 1, 3);
            this.tableLayoutPanel1.Controls.Add(this.btnTest, 2, 3);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(4, 4);
            this.tableLayoutPanel1.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 4;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 31F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 28F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 28F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 34F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(725, 134);
            this.tableLayoutPanel1.TabIndex = 0;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(4, 0);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(107, 31);
            this.label1.TabIndex = 0;
            this.label1.Text = "Database Server/Instance";
            // 
            // txtDatabaseServer
            // 
            this.txtDatabaseServer.Location = new System.Drawing.Point(141, 4);
            this.txtDatabaseServer.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.txtDatabaseServer.Name = "txtDatabaseServer";
            this.txtDatabaseServer.Size = new System.Drawing.Size(325, 22);
            this.txtDatabaseServer.TabIndex = 1;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(4, 31);
            this.label2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(110, 17);
            this.label2.TabIndex = 3;
            this.label2.Text = "Database Name";
            // 
            // txtDatabaseName
            // 
            this.txtDatabaseName.Location = new System.Drawing.Point(141, 35);
            this.txtDatabaseName.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.txtDatabaseName.Name = "txtDatabaseName";
            this.txtDatabaseName.Size = new System.Drawing.Size(255, 22);
            this.txtDatabaseName.TabIndex = 2;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(4, 59);
            this.label3.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(79, 17);
            this.label3.TabIndex = 4;
            this.label3.Text = "User Name";
            // 
            // txtUserName
            // 
            this.txtUserName.Location = new System.Drawing.Point(141, 63);
            this.txtUserName.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.txtUserName.Name = "txtUserName";
            this.txtUserName.Size = new System.Drawing.Size(255, 22);
            this.txtUserName.TabIndex = 5;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(4, 87);
            this.label4.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(69, 17);
            this.label4.TabIndex = 6;
            this.label4.Text = "Password";
            // 
            // txtPassword
            // 
            this.txtPassword.Location = new System.Drawing.Point(141, 91);
            this.txtPassword.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.txtPassword.Name = "txtPassword";
            this.txtPassword.Size = new System.Drawing.Size(255, 22);
            this.txtPassword.TabIndex = 7;
            // 
            // btnTest
            // 
            this.btnTest.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnTest.Location = new System.Drawing.Point(570, 102);
            this.btnTest.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.btnTest.Name = "btnTest";
            this.btnTest.Size = new System.Drawing.Size(151, 28);
            this.btnTest.TabIndex = 8;
            this.btnTest.Text = "&Test Connection";
            this.btnTest.UseVisualStyleBackColor = true;
            this.btnTest.Click += new System.EventHandler(this.btnTest_Click);
            // 
            // tbFileLocations
            // 
            this.tbFileLocations.Controls.Add(this.label20);
            this.tbFileLocations.Controls.Add(this.label19);
            this.tbFileLocations.Controls.Add(this.panel2);
            this.tbFileLocations.Controls.Add(this.panel1);
            this.tbFileLocations.Location = new System.Drawing.Point(4, 25);
            this.tbFileLocations.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.tbFileLocations.Name = "tbFileLocations";
            this.tbFileLocations.Padding = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.tbFileLocations.Size = new System.Drawing.Size(769, 556);
            this.tbFileLocations.TabIndex = 1;
            this.tbFileLocations.Text = "File Locations";
            this.tbFileLocations.UseVisualStyleBackColor = true;
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label20.Location = new System.Drawing.Point(28, 20);
            this.label20.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(192, 17);
            this.label20.TabIndex = 21;
            this.label20.Text = "Production File Locations";
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label19.Location = new System.Drawing.Point(35, 285);
            this.label19.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(146, 17);
            this.label19.TabIndex = 20;
            this.label19.Text = "Test File Locations";
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.Color.LightGreen;
            this.panel2.Controls.Add(this.btnOutboundLocation);
            this.panel2.Controls.Add(this.txtOutboundLocation);
            this.panel2.Controls.Add(this.label30);
            this.panel2.Controls.Add(this.btnReProcessLocation);
            this.panel2.Controls.Add(this.txtReProcessLoc);
            this.panel2.Controls.Add(this.label28);
            this.panel2.Controls.Add(this.btnPickupLocation);
            this.panel2.Controls.Add(this.txtPickupLocation);
            this.panel2.Controls.Add(this.label26);
            this.panel2.Controls.Add(this.btnArchiveLocation);
            this.panel2.Controls.Add(this.txtArchiveLocation);
            this.panel2.Controls.Add(this.label16);
            this.panel2.Controls.Add(this.btnPrimaryLocation);
            this.panel2.Controls.Add(this.txtPrimaryLocation);
            this.panel2.Controls.Add(this.label17);
            this.panel2.Controls.Add(this.btnTempLocation);
            this.panel2.Controls.Add(this.txtTempLocation);
            this.panel2.Controls.Add(this.label18);
            this.panel2.Location = new System.Drawing.Point(28, 39);
            this.panel2.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(684, 217);
            this.panel2.TabIndex = 19;
            // 
            // btnReProcessLocation
            // 
            this.btnReProcessLocation.Location = new System.Drawing.Point(636, 139);
            this.btnReProcessLocation.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.btnReProcessLocation.Name = "btnReProcessLocation";
            this.btnReProcessLocation.Size = new System.Drawing.Size(33, 25);
            this.btnReProcessLocation.TabIndex = 23;
            this.btnReProcessLocation.Text = "...";
            this.btnReProcessLocation.UseVisualStyleBackColor = true;
            // 
            // txtReProcessLoc
            // 
            this.txtReProcessLoc.Location = new System.Drawing.Point(196, 139);
            this.txtReProcessLoc.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.txtReProcessLoc.Name = "txtReProcessLoc";
            this.txtReProcessLoc.Size = new System.Drawing.Size(439, 22);
            this.txtReProcessLoc.TabIndex = 22;
            // 
            // label28
            // 
            this.label28.AutoSize = true;
            this.label28.Location = new System.Drawing.Point(12, 143);
            this.label28.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(129, 17);
            this.label28.TabIndex = 21;
            this.label28.Text = "Re-Process Queue";
            // 
            // btnPickupLocation
            // 
            this.btnPickupLocation.Location = new System.Drawing.Point(636, 108);
            this.btnPickupLocation.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.btnPickupLocation.Name = "btnPickupLocation";
            this.btnPickupLocation.Size = new System.Drawing.Size(33, 25);
            this.btnPickupLocation.TabIndex = 20;
            this.btnPickupLocation.Text = "...";
            this.btnPickupLocation.UseVisualStyleBackColor = true;
            this.btnPickupLocation.Click += new System.EventHandler(this.btnPickupLocation_Click);
            // 
            // txtPickupLocation
            // 
            this.txtPickupLocation.Location = new System.Drawing.Point(196, 108);
            this.txtPickupLocation.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.txtPickupLocation.Name = "txtPickupLocation";
            this.txtPickupLocation.Size = new System.Drawing.Size(439, 22);
            this.txtPickupLocation.TabIndex = 19;
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.Location = new System.Drawing.Point(12, 112);
            this.label26.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(179, 17);
            this.label26.TabIndex = 18;
            this.label26.Text = "CTC Adaptor Pickup Folder";
            // 
            // btnArchiveLocation
            // 
            this.btnArchiveLocation.Location = new System.Drawing.Point(636, 76);
            this.btnArchiveLocation.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.btnArchiveLocation.Name = "btnArchiveLocation";
            this.btnArchiveLocation.Size = new System.Drawing.Size(33, 25);
            this.btnArchiveLocation.TabIndex = 17;
            this.btnArchiveLocation.Text = "...";
            this.btnArchiveLocation.UseVisualStyleBackColor = true;
            this.btnArchiveLocation.Click += new System.EventHandler(this.btnArchiveLocation_Click);
            // 
            // txtArchiveLocation
            // 
            this.txtArchiveLocation.Location = new System.Drawing.Point(196, 76);
            this.txtArchiveLocation.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.txtArchiveLocation.Name = "txtArchiveLocation";
            this.txtArchiveLocation.Size = new System.Drawing.Size(439, 22);
            this.txtArchiveLocation.TabIndex = 16;
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(12, 80);
            this.label16.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(113, 17);
            this.label16.TabIndex = 15;
            this.label16.Text = "Archive Location";
            // 
            // btnPrimaryLocation
            // 
            this.btnPrimaryLocation.Location = new System.Drawing.Point(636, 44);
            this.btnPrimaryLocation.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.btnPrimaryLocation.Name = "btnPrimaryLocation";
            this.btnPrimaryLocation.Size = new System.Drawing.Size(33, 25);
            this.btnPrimaryLocation.TabIndex = 14;
            this.btnPrimaryLocation.Text = "...";
            this.btnPrimaryLocation.UseVisualStyleBackColor = true;
            this.btnPrimaryLocation.Click += new System.EventHandler(this.btnPrimaryLocation_Click);
            // 
            // txtPrimaryLocation
            // 
            this.txtPrimaryLocation.Location = new System.Drawing.Point(196, 44);
            this.txtPrimaryLocation.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.txtPrimaryLocation.Name = "txtPrimaryLocation";
            this.txtPrimaryLocation.Size = new System.Drawing.Size(439, 22);
            this.txtPrimaryLocation.TabIndex = 13;
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(12, 48);
            this.label17.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(140, 17);
            this.label17.TabIndex = 12;
            this.label17.Text = "Primary File Location";
            // 
            // btnTempLocation
            // 
            this.btnTempLocation.Location = new System.Drawing.Point(636, 12);
            this.btnTempLocation.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.btnTempLocation.Name = "btnTempLocation";
            this.btnTempLocation.Size = new System.Drawing.Size(33, 25);
            this.btnTempLocation.TabIndex = 11;
            this.btnTempLocation.Text = "...";
            this.btnTempLocation.UseVisualStyleBackColor = true;
            this.btnTempLocation.Click += new System.EventHandler(this.btnTempLocation_Click);
            // 
            // txtTempLocation
            // 
            this.txtTempLocation.Location = new System.Drawing.Point(196, 12);
            this.txtTempLocation.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.txtTempLocation.Name = "txtTempLocation";
            this.txtTempLocation.Size = new System.Drawing.Size(439, 22);
            this.txtTempLocation.TabIndex = 10;
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Location = new System.Drawing.Point(12, 16);
            this.label18.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(161, 17);
            this.label18.TabIndex = 9;
            this.label18.Text = "Temporary File Location";
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.PapayaWhip;
            this.panel1.Controls.Add(this.btnTestOutboundLocation);
            this.panel1.Controls.Add(this.txtTestOutboundLocation);
            this.panel1.Controls.Add(this.label31);
            this.panel1.Controls.Add(this.btnTestReProcessQueue);
            this.panel1.Controls.Add(this.txtTestReProcessLocation);
            this.panel1.Controls.Add(this.label29);
            this.panel1.Controls.Add(this.btnTestPickupLocation);
            this.panel1.Controls.Add(this.txtTestPickupLocation);
            this.panel1.Controls.Add(this.label25);
            this.panel1.Controls.Add(this.btnTestArchiveLocation);
            this.panel1.Controls.Add(this.txtTestArchiveLocation);
            this.panel1.Controls.Add(this.label8);
            this.panel1.Controls.Add(this.btnTestPrimaryLocation);
            this.panel1.Controls.Add(this.txtTestPrimaryLocation);
            this.panel1.Controls.Add(this.label7);
            this.panel1.Controls.Add(this.btnTestTempLocation);
            this.panel1.Controls.Add(this.txtTestTempLocation);
            this.panel1.Controls.Add(this.label6);
            this.panel1.Location = new System.Drawing.Point(36, 309);
            this.panel1.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(683, 216);
            this.panel1.TabIndex = 18;
            // 
            // btnTestReProcessQueue
            // 
            this.btnTestReProcessQueue.Location = new System.Drawing.Point(635, 139);
            this.btnTestReProcessQueue.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.btnTestReProcessQueue.Name = "btnTestReProcessQueue";
            this.btnTestReProcessQueue.Size = new System.Drawing.Size(33, 25);
            this.btnTestReProcessQueue.TabIndex = 14;
            this.btnTestReProcessQueue.Text = "...";
            this.btnTestReProcessQueue.UseVisualStyleBackColor = true;
            // 
            // txtTestReProcessLocation
            // 
            this.txtTestReProcessLocation.Location = new System.Drawing.Point(195, 139);
            this.txtTestReProcessLocation.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.txtTestReProcessLocation.Name = "txtTestReProcessLocation";
            this.txtTestReProcessLocation.Size = new System.Drawing.Size(439, 22);
            this.txtTestReProcessLocation.TabIndex = 13;
            // 
            // label29
            // 
            this.label29.AutoSize = true;
            this.label29.Location = new System.Drawing.Point(7, 143);
            this.label29.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label29.Name = "label29";
            this.label29.Size = new System.Drawing.Size(129, 17);
            this.label29.TabIndex = 12;
            this.label29.Text = "Re-Process Queue";
            // 
            // btnTestPickupLocation
            // 
            this.btnTestPickupLocation.Location = new System.Drawing.Point(635, 108);
            this.btnTestPickupLocation.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.btnTestPickupLocation.Name = "btnTestPickupLocation";
            this.btnTestPickupLocation.Size = new System.Drawing.Size(33, 25);
            this.btnTestPickupLocation.TabIndex = 11;
            this.btnTestPickupLocation.Text = "...";
            this.btnTestPickupLocation.UseVisualStyleBackColor = true;
            this.btnTestPickupLocation.Click += new System.EventHandler(this.btnTestPickupLocation_Click);
            // 
            // txtTestPickupLocation
            // 
            this.txtTestPickupLocation.Location = new System.Drawing.Point(195, 108);
            this.txtTestPickupLocation.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.txtTestPickupLocation.Name = "txtTestPickupLocation";
            this.txtTestPickupLocation.Size = new System.Drawing.Size(439, 22);
            this.txtTestPickupLocation.TabIndex = 10;
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.Location = new System.Drawing.Point(7, 112);
            this.label25.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(179, 17);
            this.label25.TabIndex = 9;
            this.label25.Text = "CTC Adaptor Pickup Folder";
            // 
            // btnTestArchiveLocation
            // 
            this.btnTestArchiveLocation.Location = new System.Drawing.Point(635, 76);
            this.btnTestArchiveLocation.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.btnTestArchiveLocation.Name = "btnTestArchiveLocation";
            this.btnTestArchiveLocation.Size = new System.Drawing.Size(33, 25);
            this.btnTestArchiveLocation.TabIndex = 8;
            this.btnTestArchiveLocation.Text = "...";
            this.btnTestArchiveLocation.UseVisualStyleBackColor = true;
            this.btnTestArchiveLocation.Click += new System.EventHandler(this.btnTestArchive_Click);
            // 
            // txtTestArchiveLocation
            // 
            this.txtTestArchiveLocation.Location = new System.Drawing.Point(195, 76);
            this.txtTestArchiveLocation.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.txtTestArchiveLocation.Name = "txtTestArchiveLocation";
            this.txtTestArchiveLocation.Size = new System.Drawing.Size(439, 22);
            this.txtTestArchiveLocation.TabIndex = 7;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(7, 80);
            this.label8.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(113, 17);
            this.label8.TabIndex = 6;
            this.label8.Text = "Archive Location";
            // 
            // btnTestPrimaryLocation
            // 
            this.btnTestPrimaryLocation.Location = new System.Drawing.Point(635, 44);
            this.btnTestPrimaryLocation.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.btnTestPrimaryLocation.Name = "btnTestPrimaryLocation";
            this.btnTestPrimaryLocation.Size = new System.Drawing.Size(33, 25);
            this.btnTestPrimaryLocation.TabIndex = 5;
            this.btnTestPrimaryLocation.Text = "...";
            this.btnTestPrimaryLocation.UseVisualStyleBackColor = true;
            this.btnTestPrimaryLocation.Click += new System.EventHandler(this.btnTestPrimaryLocation_Click);
            // 
            // txtTestPrimaryLocation
            // 
            this.txtTestPrimaryLocation.Location = new System.Drawing.Point(195, 44);
            this.txtTestPrimaryLocation.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.txtTestPrimaryLocation.Name = "txtTestPrimaryLocation";
            this.txtTestPrimaryLocation.Size = new System.Drawing.Size(439, 22);
            this.txtTestPrimaryLocation.TabIndex = 4;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(7, 48);
            this.label7.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(140, 17);
            this.label7.TabIndex = 3;
            this.label7.Text = "Primary File Location";
            // 
            // btnTestTempLocation
            // 
            this.btnTestTempLocation.Location = new System.Drawing.Point(635, 12);
            this.btnTestTempLocation.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.btnTestTempLocation.Name = "btnTestTempLocation";
            this.btnTestTempLocation.Size = new System.Drawing.Size(33, 25);
            this.btnTestTempLocation.TabIndex = 2;
            this.btnTestTempLocation.Text = "...";
            this.btnTestTempLocation.UseVisualStyleBackColor = true;
            this.btnTestTempLocation.Click += new System.EventHandler(this.btnTestTempLocation_Click);
            // 
            // txtTestTempLocation
            // 
            this.txtTestTempLocation.Location = new System.Drawing.Point(195, 12);
            this.txtTestTempLocation.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.txtTestTempLocation.Name = "txtTestTempLocation";
            this.txtTestTempLocation.Size = new System.Drawing.Size(439, 22);
            this.txtTestTempLocation.TabIndex = 1;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(7, 16);
            this.label6.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(161, 17);
            this.label6.TabIndex = 0;
            this.label6.Text = "Temporary File Location";
            // 
            // tbCommunications
            // 
            this.tbCommunications.Controls.Add(this.cbSMTPSecure);
            this.tbCommunications.Controls.Add(this.txtSmtpEmailAddress);
            this.tbCommunications.Controls.Add(this.label13);
            this.tbCommunications.Controls.Add(this.txtSmtpPassword);
            this.tbCommunications.Controls.Add(this.label12);
            this.tbCommunications.Controls.Add(this.txtSmtpUserName);
            this.tbCommunications.Controls.Add(this.label11);
            this.tbCommunications.Controls.Add(this.txtSmtpPort);
            this.tbCommunications.Controls.Add(this.label10);
            this.tbCommunications.Controls.Add(this.txtSmtpServer);
            this.tbCommunications.Controls.Add(this.label9);
            this.tbCommunications.Location = new System.Drawing.Point(4, 25);
            this.tbCommunications.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.tbCommunications.Name = "tbCommunications";
            this.tbCommunications.Padding = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.tbCommunications.Size = new System.Drawing.Size(733, 436);
            this.tbCommunications.TabIndex = 2;
            this.tbCommunications.Text = "Communications";
            this.tbCommunications.UseVisualStyleBackColor = true;
            // 
            // cbSMTPSecure
            // 
            this.cbSMTPSecure.AutoSize = true;
            this.cbSMTPSecure.Location = new System.Drawing.Point(303, 68);
            this.cbSMTPSecure.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.cbSMTPSecure.Name = "cbSMTPSecure";
            this.cbSMTPSecure.Size = new System.Drawing.Size(105, 21);
            this.cbSMTPSecure.TabIndex = 10;
            this.cbSMTPSecure.Text = "Secure SSL";
            this.cbSMTPSecure.UseVisualStyleBackColor = true;
            // 
            // txtSmtpEmailAddress
            // 
            this.txtSmtpEmailAddress.Location = new System.Drawing.Point(181, 140);
            this.txtSmtpEmailAddress.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.txtSmtpEmailAddress.Name = "txtSmtpEmailAddress";
            this.txtSmtpEmailAddress.Size = new System.Drawing.Size(481, 22);
            this.txtSmtpEmailAddress.TabIndex = 9;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(20, 144);
            this.label13.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(154, 17);
            this.label13.TabIndex = 8;
            this.label13.Text = "Sending Email Address";
            // 
            // txtSmtpPassword
            // 
            this.txtSmtpPassword.Location = new System.Drawing.Point(487, 108);
            this.txtSmtpPassword.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.txtSmtpPassword.Name = "txtSmtpPassword";
            this.txtSmtpPassword.Size = new System.Drawing.Size(176, 22);
            this.txtSmtpPassword.TabIndex = 7;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(397, 111);
            this.label12.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(69, 17);
            this.label12.TabIndex = 6;
            this.label12.Text = "Password";
            // 
            // txtSmtpUserName
            // 
            this.txtSmtpUserName.Location = new System.Drawing.Point(181, 107);
            this.txtSmtpUserName.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.txtSmtpUserName.Name = "txtSmtpUserName";
            this.txtSmtpUserName.Size = new System.Drawing.Size(184, 22);
            this.txtSmtpUserName.TabIndex = 5;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(20, 110);
            this.label11.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(79, 17);
            this.label11.TabIndex = 4;
            this.label11.Text = "User Name";
            // 
            // txtSmtpPort
            // 
            this.txtSmtpPort.Location = new System.Drawing.Point(181, 65);
            this.txtSmtpPort.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.txtSmtpPort.Name = "txtSmtpPort";
            this.txtSmtpPort.Size = new System.Drawing.Size(76, 22);
            this.txtSmtpPort.TabIndex = 3;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(20, 68);
            this.label10.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(76, 17);
            this.label10.TabIndex = 2;
            this.label10.Text = "SMTP Port";
            // 
            // txtSmtpServer
            // 
            this.txtSmtpServer.Location = new System.Drawing.Point(181, 33);
            this.txtSmtpServer.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.txtSmtpServer.Name = "txtSmtpServer";
            this.txtSmtpServer.Size = new System.Drawing.Size(481, 22);
            this.txtSmtpServer.TabIndex = 1;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(20, 37);
            this.label9.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(92, 17);
            this.label9.TabIndex = 0;
            this.label9.Text = "SMTP Server";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(21, 11);
            this.label15.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(159, 17);
            this.label15.TabIndex = 2;
            this.label15.Text = "Update System Settings";
            // 
            // btnSave
            // 
            this.btnSave.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnSave.Location = new System.Drawing.Point(585, 627);
            this.btnSave.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(100, 28);
            this.btnSave.TabIndex = 3;
            this.btnSave.Text = "&Save";
            this.btnSave.UseVisualStyleBackColor = true;
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // label30
            // 
            this.label30.AutoSize = true;
            this.label30.Location = new System.Drawing.Point(12, 175);
            this.label30.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label30.Name = "label30";
            this.label30.Size = new System.Drawing.Size(129, 17);
            this.label30.TabIndex = 24;
            this.label30.Text = "Outbound Location";
            // 
            // txtOutboundLocation
            // 
            this.txtOutboundLocation.Location = new System.Drawing.Point(196, 172);
            this.txtOutboundLocation.Margin = new System.Windows.Forms.Padding(4);
            this.txtOutboundLocation.Name = "txtOutboundLocation";
            this.txtOutboundLocation.Size = new System.Drawing.Size(439, 22);
            this.txtOutboundLocation.TabIndex = 25;
            // 
            // btnOutboundLocation
            // 
            this.btnOutboundLocation.Location = new System.Drawing.Point(636, 171);
            this.btnOutboundLocation.Margin = new System.Windows.Forms.Padding(4);
            this.btnOutboundLocation.Name = "btnOutboundLocation";
            this.btnOutboundLocation.Size = new System.Drawing.Size(33, 25);
            this.btnOutboundLocation.TabIndex = 26;
            this.btnOutboundLocation.Text = "...";
            this.btnOutboundLocation.UseVisualStyleBackColor = true;
            this.btnOutboundLocation.Click += new System.EventHandler(this.btnOutboundLocation_Click);
            // 
            // label31
            // 
            this.label31.AutoSize = true;
            this.label31.Location = new System.Drawing.Point(7, 174);
            this.label31.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label31.Name = "label31";
            this.label31.Size = new System.Drawing.Size(129, 17);
            this.label31.TabIndex = 15;
            this.label31.Text = "Outbound Location";
            // 
            // txtTestOutboundLocation
            // 
            this.txtTestOutboundLocation.Location = new System.Drawing.Point(195, 171);
            this.txtTestOutboundLocation.Margin = new System.Windows.Forms.Padding(4);
            this.txtTestOutboundLocation.Name = "txtTestOutboundLocation";
            this.txtTestOutboundLocation.Size = new System.Drawing.Size(439, 22);
            this.txtTestOutboundLocation.TabIndex = 16;
            // 
            // btnTestOutboundLocation
            // 
            this.btnTestOutboundLocation.Location = new System.Drawing.Point(635, 170);
            this.btnTestOutboundLocation.Margin = new System.Windows.Forms.Padding(4);
            this.btnTestOutboundLocation.Name = "btnTestOutboundLocation";
            this.btnTestOutboundLocation.Size = new System.Drawing.Size(33, 25);
            this.btnTestOutboundLocation.TabIndex = 17;
            this.btnTestOutboundLocation.Text = "...";
            this.btnTestOutboundLocation.UseVisualStyleBackColor = true;
            this.btnTestOutboundLocation.Click += new System.EventHandler(this.btnTestOutboundLocation_Click);
            // 
            // Settings
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(809, 670);
            this.Controls.Add(this.btnSave);
            this.Controls.Add(this.label15);
            this.Controls.Add(this.tcSettings);
            this.Controls.Add(this.btnClose);
            this.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.Name = "Settings";
            this.Text = "Settings";
            this.Load += new System.EventHandler(this.Settings_Load);
            this.tcSettings.ResumeLayout(false);
            this.tbSettings.ResumeLayout(false);
            this.tbSettings.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtRefresh)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPollTime)).EndInit();
            this.tbDatabase.ResumeLayout(false);
            this.tableLayoutPanel2.ResumeLayout(false);
            this.tableLayoutPanel2.PerformLayout();
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel1.PerformLayout();
            this.tbFileLocations.ResumeLayout(false);
            this.tbFileLocations.PerformLayout();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.tbCommunications.ResumeLayout(false);
            this.tbCommunications.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnClose;
        private System.Windows.Forms.TabControl tcSettings;
        private System.Windows.Forms.TabPage tbDatabase;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel2;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox txtConnectionResults;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txtDatabaseServer;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txtDatabaseName;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox txtUserName;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox txtPassword;
        private System.Windows.Forms.Button btnTest;
        private System.Windows.Forms.TabPage tbFileLocations;
        private System.Windows.Forms.TabPage tbCommunications;
        private System.Windows.Forms.Button btnTestArchiveLocation;
        private System.Windows.Forms.TextBox txtTestArchiveLocation;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Button btnTestPrimaryLocation;
        private System.Windows.Forms.TextBox txtTestPrimaryLocation;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Button btnTestTempLocation;
        private System.Windows.Forms.TextBox txtTestTempLocation;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox txtSmtpEmailAddress;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.TextBox txtSmtpPassword;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.TextBox txtSmtpUserName;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.TextBox txtSmtpPort;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.TextBox txtSmtpServer;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Button btnSave;
        private System.Windows.Forms.TabPage tbSettings;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Button btnArchiveLocation;
        private System.Windows.Forms.TextBox txtArchiveLocation;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Button btnPrimaryLocation;
        private System.Windows.Forms.TextBox txtPrimaryLocation;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Button btnTempLocation;
        private System.Windows.Forms.TextBox txtTempLocation;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.NumericUpDown txtPollTime;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.NumericUpDown txtRefresh;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.TextBox txtAlertsTo;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.CheckBox cbSMTPSecure;
        private System.Windows.Forms.Button btnPickupLocation;
        private System.Windows.Forms.TextBox txtPickupLocation;
        private System.Windows.Forms.Label label26;
        private System.Windows.Forms.Button btnTestPickupLocation;
        private System.Windows.Forms.TextBox txtTestPickupLocation;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.TextBox edWinSCP;
        private System.Windows.Forms.Label label27;
        private System.Windows.Forms.Button btnReProcessLocation;
        private System.Windows.Forms.TextBox txtReProcessLoc;
        private System.Windows.Forms.Label label28;
        private System.Windows.Forms.Button btnTestReProcessQueue;
        private System.Windows.Forms.TextBox txtTestReProcessLocation;
        private System.Windows.Forms.Label label29;
        private System.Windows.Forms.Button btnOutboundLocation;
        private System.Windows.Forms.TextBox txtOutboundLocation;
        private System.Windows.Forms.Label label30;
        private System.Windows.Forms.Button btnTestOutboundLocation;
        private System.Windows.Forms.TextBox txtTestOutboundLocation;
        private System.Windows.Forms.Label label31;
    }
}