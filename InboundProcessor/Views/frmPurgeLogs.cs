﻿using InboundProcessor.Classes;
using System;
using System.Reflection;
using System.Windows.Forms;

namespace InboundProcessor.Views
{
    public partial class frmPurgeLogs : Form
    {
        public frmPurgeLogs()
        {
            InitializeComponent();
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnPurge_Click(object sender, EventArgs e)
        {
            using (AppLogging log = new AppLogging(Globals.ConnString.ConnString))
            {
                log.AppName = Assembly.GetExecutingAssembly().GetName().Name;
                if (log.PurgeLog(cbAll.Checked, dtPurge.Value, cbInformation.Checked, cbWarning.Checked, cbError.Checked))
                {
                    MessageBox.Show("Log Messages purged");
                }
            }
        }
    }
}
