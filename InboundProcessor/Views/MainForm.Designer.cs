﻿namespace InboundProcessor
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainForm));
            this.btnClose = new System.Windows.Forms.Button();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.fileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.exitToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.systemToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.settingsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.purgeLogsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.helpToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.btnStart = new System.Windows.Forms.Button();
            this.statusStrip1 = new System.Windows.Forms.StatusStrip();
            this.tslMain = new System.Windows.Forms.ToolStripStatusLabel();
            this.tslSpacer = new System.Windows.Forms.ToolStripStatusLabel();
            this.tslMode = new System.Windows.Forms.ToolStripStatusLabel();
            this.tslCmbMode = new System.Windows.Forms.ToolStripDropDownButton();
            this.testingToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.productionToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.gbLogging = new System.Windows.Forms.GroupBox();
            this.rbFullLog = new System.Windows.Forms.RadioButton();
            this.rbMinimal = new System.Windows.Forms.RadioButton();
            this.rbNone = new System.Windows.Forms.RadioButton();
            this.gbOperations = new System.Windows.Forms.GroupBox();
            this.cbFolder = new System.Windows.Forms.CheckBox();
            this.cbEadaptor = new System.Windows.Forms.CheckBox();
            this.cbFtp = new System.Windows.Forms.CheckBox();
            this.cbEmail = new System.Windows.Forms.CheckBox();
            this.ilMain = new System.Windows.Forms.ImageList(this.components);
            this.menuStrip1.SuspendLayout();
            this.statusStrip1.SuspendLayout();
            this.gbLogging.SuspendLayout();
            this.gbOperations.SuspendLayout();
            this.SuspendLayout();
            // 
            // btnClose
            // 
            this.btnClose.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnClose.Location = new System.Drawing.Point(380, 121);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(75, 23);
            this.btnClose.TabIndex = 0;
            this.btnClose.Text = "&Close";
            this.btnClose.UseVisualStyleBackColor = true;
            this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.fileToolStripMenuItem,
            this.systemToolStripMenuItem,
            this.helpToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(467, 24);
            this.menuStrip1.TabIndex = 1;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // fileToolStripMenuItem
            // 
            this.fileToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.exitToolStripMenuItem});
            this.fileToolStripMenuItem.Name = "fileToolStripMenuItem";
            this.fileToolStripMenuItem.Size = new System.Drawing.Size(37, 20);
            this.fileToolStripMenuItem.Text = "&File";
            // 
            // exitToolStripMenuItem
            // 
            this.exitToolStripMenuItem.Name = "exitToolStripMenuItem";
            this.exitToolStripMenuItem.Size = new System.Drawing.Size(93, 22);
            this.exitToolStripMenuItem.Text = "E&xit";
            this.exitToolStripMenuItem.Click += new System.EventHandler(this.exitToolStripMenuItem_Click);
            // 
            // systemToolStripMenuItem
            // 
            this.systemToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.settingsToolStripMenuItem,
            this.purgeLogsToolStripMenuItem});
            this.systemToolStripMenuItem.Name = "systemToolStripMenuItem";
            this.systemToolStripMenuItem.Size = new System.Drawing.Size(57, 20);
            this.systemToolStripMenuItem.Text = "&System";
            // 
            // settingsToolStripMenuItem
            // 
            this.settingsToolStripMenuItem.Name = "settingsToolStripMenuItem";
            this.settingsToolStripMenuItem.Size = new System.Drawing.Size(133, 22);
            this.settingsToolStripMenuItem.Text = "S&ettings";
            this.settingsToolStripMenuItem.Click += new System.EventHandler(this.settingsToolStripMenuItem_Click);
            // 
            // purgeLogsToolStripMenuItem
            // 
            this.purgeLogsToolStripMenuItem.Name = "purgeLogsToolStripMenuItem";
            this.purgeLogsToolStripMenuItem.Size = new System.Drawing.Size(133, 22);
            this.purgeLogsToolStripMenuItem.Text = "Purge &Logs";
            this.purgeLogsToolStripMenuItem.Click += new System.EventHandler(this.purgeLogsToolStripMenuItem_Click);
            // 
            // helpToolStripMenuItem
            // 
            this.helpToolStripMenuItem.Name = "helpToolStripMenuItem";
            this.helpToolStripMenuItem.Size = new System.Drawing.Size(44, 20);
            this.helpToolStripMenuItem.Text = "&Help";
            // 
            // btnStart
            // 
            this.btnStart.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnStart.Location = new System.Drawing.Point(380, 94);
            this.btnStart.Name = "btnStart";
            this.btnStart.Size = new System.Drawing.Size(75, 23);
            this.btnStart.TabIndex = 7;
            this.btnStart.Text = "&Start";
            this.btnStart.UseVisualStyleBackColor = true;
            this.btnStart.Click += new System.EventHandler(this.btnStart_Click);
            // 
            // statusStrip1
            // 
            this.statusStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tslMain,
            this.tslSpacer,
            this.tslMode,
            this.tslCmbMode});
            this.statusStrip1.Location = new System.Drawing.Point(0, 153);
            this.statusStrip1.Name = "statusStrip1";
            this.statusStrip1.Size = new System.Drawing.Size(467, 22);
            this.statusStrip1.TabIndex = 8;
            this.statusStrip1.Text = "statusStrip1";
            // 
            // tslMain
            // 
            this.tslMain.AutoSize = false;
            this.tslMain.Name = "tslMain";
            this.tslMain.Size = new System.Drawing.Size(39, 17);
            this.tslMain.Text = "Status";
            this.tslMain.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // tslSpacer
            // 
            this.tslSpacer.AutoSize = false;
            this.tslSpacer.Name = "tslSpacer";
            this.tslSpacer.Padding = new System.Windows.Forms.Padding(50, 0, 0, 0);
            this.tslSpacer.Size = new System.Drawing.Size(50, 17);
            // 
            // tslMode
            // 
            this.tslMode.AutoSize = false;
            this.tslMode.Name = "tslMode";
            this.tslMode.Padding = new System.Windows.Forms.Padding(50, 0, 0, 0);
            this.tslMode.Size = new System.Drawing.Size(50, 17);
            this.tslMode.Text = "Mode";
            // 
            // tslCmbMode
            // 
            this.tslCmbMode.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.tslCmbMode.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.testingToolStripMenuItem,
            this.productionToolStripMenuItem});
            this.tslCmbMode.Image = ((System.Drawing.Image)(resources.GetObject("tslCmbMode.Image")));
            this.tslCmbMode.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tslCmbMode.Name = "tslCmbMode";
            this.tslCmbMode.Overflow = System.Windows.Forms.ToolStripItemOverflow.Never;
            this.tslCmbMode.Padding = new System.Windows.Forms.Padding(0, 0, 15, 0);
            this.tslCmbMode.Size = new System.Drawing.Size(44, 20);
            this.tslCmbMode.Text = "toolStripDropDownButton1";
            // 
            // testingToolStripMenuItem
            // 
            this.testingToolStripMenuItem.CheckOnClick = true;
            this.testingToolStripMenuItem.Name = "testingToolStripMenuItem";
            this.testingToolStripMenuItem.Size = new System.Drawing.Size(133, 22);
            this.testingToolStripMenuItem.Text = "Testing";
            this.testingToolStripMenuItem.Click += new System.EventHandler(this.TestingToolStripMenuItem_Click);
            // 
            // productionToolStripMenuItem
            // 
            this.productionToolStripMenuItem.Checked = true;
            this.productionToolStripMenuItem.CheckOnClick = true;
            this.productionToolStripMenuItem.CheckState = System.Windows.Forms.CheckState.Checked;
            this.productionToolStripMenuItem.Name = "productionToolStripMenuItem";
            this.productionToolStripMenuItem.Size = new System.Drawing.Size(133, 22);
            this.productionToolStripMenuItem.Text = "Production";
            this.productionToolStripMenuItem.Click += new System.EventHandler(this.productionToolStripMenuItem_Click);
            // 
            // gbLogging
            // 
            this.gbLogging.Controls.Add(this.rbFullLog);
            this.gbLogging.Controls.Add(this.rbMinimal);
            this.gbLogging.Controls.Add(this.rbNone);
            this.gbLogging.Location = new System.Drawing.Point(206, 47);
            this.gbLogging.Name = "gbLogging";
            this.gbLogging.Size = new System.Drawing.Size(170, 102);
            this.gbLogging.TabIndex = 9;
            this.gbLogging.TabStop = false;
            this.gbLogging.Text = "Logging Level";
            // 
            // rbFullLog
            // 
            this.rbFullLog.AutoSize = true;
            this.rbFullLog.Location = new System.Drawing.Point(12, 70);
            this.rbFullLog.Name = "rbFullLog";
            this.rbFullLog.Size = new System.Drawing.Size(41, 17);
            this.rbFullLog.TabIndex = 2;
            this.rbFullLog.Tag = "2";
            this.rbFullLog.Text = "Full";
            this.rbFullLog.UseVisualStyleBackColor = true;
            // 
            // rbMinimal
            // 
            this.rbMinimal.AutoSize = true;
            this.rbMinimal.Checked = true;
            this.rbMinimal.Location = new System.Drawing.Point(12, 47);
            this.rbMinimal.Name = "rbMinimal";
            this.rbMinimal.Size = new System.Drawing.Size(118, 17);
            this.rbMinimal.TabIndex = 1;
            this.rbMinimal.TabStop = true;
            this.rbMinimal.Tag = "1";
            this.rbMinimal.Text = "Minimal - Errors only";
            this.rbMinimal.UseVisualStyleBackColor = true;
            // 
            // rbNone
            // 
            this.rbNone.AutoSize = true;
            this.rbNone.Location = new System.Drawing.Point(13, 24);
            this.rbNone.Name = "rbNone";
            this.rbNone.Size = new System.Drawing.Size(51, 17);
            this.rbNone.TabIndex = 0;
            this.rbNone.Tag = "0";
            this.rbNone.Text = "None";
            this.rbNone.UseVisualStyleBackColor = true;
            // 
            // gbOperations
            // 
            this.gbOperations.Controls.Add(this.cbFolder);
            this.gbOperations.Controls.Add(this.cbEadaptor);
            this.gbOperations.Controls.Add(this.cbFtp);
            this.gbOperations.Controls.Add(this.cbEmail);
            this.gbOperations.Location = new System.Drawing.Point(12, 36);
            this.gbOperations.Name = "gbOperations";
            this.gbOperations.Size = new System.Drawing.Size(170, 113);
            this.gbOperations.TabIndex = 10;
            this.gbOperations.TabStop = false;
            this.gbOperations.Text = "Process Operations";
            // 
            // cbFolder
            // 
            this.cbFolder.AutoSize = true;
            this.cbFolder.Location = new System.Drawing.Point(14, 89);
            this.cbFolder.Name = "cbFolder";
            this.cbFolder.Size = new System.Drawing.Size(129, 17);
            this.cbFolder.TabIndex = 10;
            this.cbFolder.Text = "Check Folder Queues";
            this.cbFolder.UseVisualStyleBackColor = true;
            // 
            // cbEadaptor
            // 
            this.cbEadaptor.AutoSize = true;
            this.cbEadaptor.Cursor = System.Windows.Forms.Cursors.AppStarting;
            this.cbEadaptor.Location = new System.Drawing.Point(14, 66);
            this.cbEadaptor.Name = "cbEadaptor";
            this.cbEadaptor.Size = new System.Drawing.Size(143, 17);
            this.cbEadaptor.TabIndex = 9;
            this.cbEadaptor.Text = "Check eAdaptor Queues";
            this.cbEadaptor.UseVisualStyleBackColor = true;
            // 
            // cbFtp
            // 
            this.cbFtp.AutoSize = true;
            this.cbFtp.Location = new System.Drawing.Point(14, 43);
            this.cbFtp.Name = "cbFtp";
            this.cbFtp.Size = new System.Drawing.Size(120, 17);
            this.cbFtp.TabIndex = 8;
            this.cbFtp.Text = "Check FTP Queues";
            this.cbFtp.UseVisualStyleBackColor = true;
            // 
            // cbEmail
            // 
            this.cbEmail.AutoSize = true;
            this.cbEmail.Location = new System.Drawing.Point(14, 20);
            this.cbEmail.Name = "cbEmail";
            this.cbEmail.Size = new System.Drawing.Size(125, 17);
            this.cbEmail.TabIndex = 7;
            this.cbEmail.Text = "Check Email Queues";
            this.cbEmail.UseVisualStyleBackColor = true;
            // 
            // ilMain
            // 
            this.ilMain.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("ilMain.ImageStream")));
            this.ilMain.TransparentColor = System.Drawing.Color.Transparent;
            this.ilMain.Images.SetKeyName(0, "Error");
            this.ilMain.Images.SetKeyName(1, "Information");
            this.ilMain.Images.SetKeyName(2, "Warning");
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(467, 175);
            this.Controls.Add(this.gbOperations);
            this.Controls.Add(this.gbLogging);
            this.Controls.Add(this.statusStrip1);
            this.Controls.Add(this.btnStart);
            this.Controls.Add(this.btnClose);
            this.Controls.Add(this.menuStrip1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "MainForm";
            this.Text = "Inbound Processor";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.MainForm_FormClosing);
            this.Load += new System.EventHandler(this.MainForm_Load);
            this.Resize += new System.EventHandler(this.MainForm_Resize);
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.statusStrip1.ResumeLayout(false);
            this.statusStrip1.PerformLayout();
            this.gbLogging.ResumeLayout(false);
            this.gbLogging.PerformLayout();
            this.gbOperations.ResumeLayout(false);
            this.gbOperations.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnClose;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem fileToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem exitToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem systemToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem settingsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem helpToolStripMenuItem;
        private System.Windows.Forms.Button btnStart;
        private System.Windows.Forms.StatusStrip statusStrip1;
        private System.Windows.Forms.ToolStripStatusLabel tslMain;
        private System.Windows.Forms.ToolStripStatusLabel tslSpacer;
        private System.Windows.Forms.ToolStripStatusLabel tslMode;
        private System.Windows.Forms.ToolStripDropDownButton tslCmbMode;
        private System.Windows.Forms.ToolStripMenuItem testingToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem productionToolStripMenuItem;
        private System.Windows.Forms.GroupBox gbLogging;
        private System.Windows.Forms.RadioButton rbFullLog;
        private System.Windows.Forms.RadioButton rbMinimal;
        private System.Windows.Forms.RadioButton rbNone;
        private System.Windows.Forms.GroupBox gbOperations;
        private System.Windows.Forms.CheckBox cbFolder;
        private System.Windows.Forms.CheckBox cbEadaptor;
        private System.Windows.Forms.CheckBox cbFtp;
        private System.Windows.Forms.CheckBox cbEmail;
        private System.Windows.Forms.ImageList ilMain;
        private System.Windows.Forms.ToolStripMenuItem purgeLogsToolStripMenuItem;
    }
}

