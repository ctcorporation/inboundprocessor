﻿
using InboundProcessor.Classes;
using NodeData;
using NodeData.DTO;
using NodeData.Models;
using System;
using System.Data.Entity.Validation;
using System.IO;
using System.Reflection;

namespace InboundProcessor
{
    public class FileOps
    {
        public string OriginalFileName { get; set; }
        public string NewFile { get; set; }
        public string ProcessFunction { get; set; }
        public string ReceivedFolder { get; set; }
        public string Subject { get; set; }
        public string FromAddress { get; set; }
        public string SenderId { get; set; }
        public string RecipientId { get; set; }
        public string PrimaryPath { get; set; }
        public string OutboundPath { get; set; }
        public string CWFile { get; set; }
        public string ToAddress { get; set; }
        public Guid ProfileID { get; set; }
        public string ArchiveName { get; set; }
        public string ErrorMessage { get; set; }
        public string AppCode { get; set; }
        public FileOps()
        {

        }
        public string NewFileName(string fileName)
        {
            FileInfo fi = new FileInfo(fileName);
            string ext = fi.Extension;
            return "CTCNode-" + Guid.NewGuid().ToString() + ext;

        }

        public bool MoveFile(string fileToMove, string newFile, string folder)
        {
            try
            {
                File.Move(fileToMove, Path.Combine(folder, newFile));
            }
            catch (Exception ex)
            {
                using (AppLogging log = new AppLogging(Globals.ConnString.ConnString))
                {
                    MethodBase m = MethodBase.GetCurrentMethod();
                    log.AddLog(new AppLog
                    {

                        GL_AppName = Assembly.GetExecutingAssembly().GetName().Name,
                        GL_ServerName = Environment.MachineName,
                        GL_Date = DateTime.Now,
                        GL_ProcName = m.Name,
                        GL_MessageType = "Error",
                        GL_MessageLevel = 2,
                        GL_MessageDetail = "Unabe to rename file " + fileToMove + Environment.NewLine +
                                            "Error: " + ex.GetType().Name + " :" + ex.Message
                    });

                }

                return false;
            }
            return true;
        }

        public bool AddFile()
        {
            bool result = false;
            string newFile = NewFileName(OriginalFileName);
            try
            {
                if (MoveFile(OriginalFileName, newFile, PrimaryPath))
                {
                    FileInfo fiOriginal = new FileInfo(OriginalFileName);
                    using (IUnitOfWork uow = new UnitOfWork(new NodeDataContext(Globals.ConnString.ConnString)))
                    {
                        TaskList tl = new TaskList();

                        tl.TL_ID = Guid.NewGuid();
                        tl.TL_OrginalFileName = fiOriginal.Name;
                        tl.TL_FileName = newFile;
                        tl.TL_ReceiveDate = DateTime.Now;
                        tl.TL_Path = PrimaryPath;
                        tl.TL_ReceiverProcess = ProcessFunction;
                        tl.TL_SenderID = SenderId;
                        tl.TL_RecipientID = RecipientId;
                        tl.TL_ReceivedFolder = ReceivedFolder;
                        tl.TL_Subject = Subject;
                        tl.TL_CWFileName = CWFile;
                        tl.TL_FromAddress = FromAddress;
                        tl.TL_ToAddress = ToAddress;
                        tl.TL_AppCode = AppCode;
                        if (ProfileID != Guid.Empty)
                        {
                            tl.TL_P = ProfileID;
                        }
                        tl.TL_ArchiveName = ArchiveName.Length > 100 ? "Duplicate Found" : ArchiveName;
                        tl.TL_Processed = false;

                        uow.TaskLists.Add(tl);
                        if (uow.Complete() != -1)
                        {
                            result = true;
                        }
                        else
                        {
                            result = false;
                            ErrorMessage = uow.ErrorMessage;
                        }
                        //result
                    }
                    //NodeDataModel _context = new NodeDataModel(Globals.ConnString.ConnString);
                    //TaskList tl = new TaskList();

                    //tl.TL_ID = Guid.NewGuid();
                    //tl.TL_OrginalFileName = fiOriginal.Name;
                    //tl.TL_FileName = newFile;
                    //tl.TL_ReceiveDate = DateTime.Now;
                    //tl.TL_Path = PrimaryPath;
                    //tl.TL_ReceiverProcess = ProcessFunction;
                    //tl.TL_SenderID = SenderId;
                    //tl.TL_RecipientID = RecipientId;
                    //tl.TL_ReceivedFolder = ReceivedFolder;
                    //tl.TL_Subject = Subject;
                    //tl.TL_CWFileName = CWFile;
                    //tl.TL_FromAddress = FromAddress;
                    //tl.TL_ToAddress = ToAddress;
                    //if (ProfileID!=Guid.Empty)
                    //{
                    //    tl.TL_P = ProfileID;
                    //}

                    //tl.TL_ArchiveName = ArchiveName;
                    //tl.TL_Processed = false;

                    //_context.TaskLists.Add(tl);
                    //_context.SaveChanges();
                    result = true;
                }
                else
                {
                    result = false;
                }
            }
            catch (DbEntityValidationException e)
            {
                foreach (var eve in e.EntityValidationErrors)
                {
                    Console.WriteLine("Entity of type \"{0}\" in state \"{1}\" has the following validation errors:",
                        eve.Entry.Entity.GetType().Name, eve.Entry.State);
                    foreach (var ve in eve.ValidationErrors)
                    {
                        Console.WriteLine("- Property: \"{0}\", Error: \"{1}\"",
                            ve.PropertyName, ve.ErrorMessage);
                    }
                }
                throw;
            }
            catch (Exception ex)
            {
                string strex = ex.GetType().Name;
            }
            return result;


        }


        public bool AddOutboundFile()
        {
            bool result = false;
            string newFile = OriginalFileName;
            try
            {
                if (MoveFile(OriginalFileName, newFile, OutboundPath))
                {
                    FileInfo fiOriginal = new FileInfo(OriginalFileName);
                    using (IUnitOfWork uow = new UnitOfWork(new NodeDataContext(Globals.ConnString.ConnString)))
                    {
                        OutboundTaskList tl = new OutboundTaskList();

                        tl.OTL_ID = Guid.NewGuid();
                        tl.OTL_OrginalFileName = fiOriginal.Name;
                        tl.OTL_FileName = newFile;
                        tl.OTL_ReceiveDate = DateTime.Now;
                        tl.OTL_Path = OutboundPath;
                        tl.OTL_ReceiverProcess = ProcessFunction;
                        tl.OTL_SenderID = SenderId;
                        tl.OTL_RecipientID = RecipientId;
                        tl.OTL_ReceivedFolder = ReceivedFolder;
                        tl.OTL_Subject = Subject;
                        tl.OTL_CWFileName = CWFile;
                        tl.OTL_FromAddress = FromAddress;
                        tl.OTL_ToAddress = ToAddress;
                        tl.OTL_AppCode = AppCode;
                        if (ProfileID != Guid.Empty)
                        {
                            tl.OTL_P = ProfileID;
                        }
                        tl.OTL_ArchiveName = ArchiveName.Length > 100 ? "Duplicate Found" : ArchiveName;
                        tl.OTL_Processed = false;

                        uow.OutboundTaskLists.Add(tl);
                        if (uow.Complete() != -1)
                        {
                            result = true;
                        }
                        else
                        {
                            result = false;
                            ErrorMessage = uow.ErrorMessage;
                        }
                        //result
                    }
                    //NodeDataModel _context = new NodeDataModel(Globals.ConnString.ConnString);
                    //TaskList tl = new TaskList();

                    //tl.TL_ID = Guid.NewGuid();
                    //tl.TL_OrginalFileName = fiOriginal.Name;
                    //tl.TL_FileName = newFile;
                    //tl.TL_ReceiveDate = DateTime.Now;
                    //tl.TL_Path = PrimaryPath;
                    //tl.TL_ReceiverProcess = ProcessFunction;
                    //tl.TL_SenderID = SenderId;
                    //tl.TL_RecipientID = RecipientId;
                    //tl.TL_ReceivedFolder = ReceivedFolder;
                    //tl.TL_Subject = Subject;
                    //tl.TL_CWFileName = CWFile;
                    //tl.TL_FromAddress = FromAddress;
                    //tl.TL_ToAddress = ToAddress;
                    //if (ProfileID!=Guid.Empty)
                    //{
                    //    tl.TL_P = ProfileID;
                    //}

                    //tl.TL_ArchiveName = ArchiveName;
                    //tl.TL_Processed = false;

                    //_context.TaskLists.Add(tl);
                    //_context.SaveChanges();
                    result = true;
                }
                else
                {
                    result = false;
                }
            }
            catch (DbEntityValidationException e)
            {
                foreach (var eve in e.EntityValidationErrors)
                {
                    Console.WriteLine("Entity of type \"{0}\" in state \"{1}\" has the following validation errors:",
                        eve.Entry.Entity.GetType().Name, eve.Entry.State);
                    foreach (var ve in eve.ValidationErrors)
                    {
                        Console.WriteLine("- Property: \"{0}\", Error: \"{1}\"",
                            ve.PropertyName, ve.ErrorMessage);
                    }
                }
                throw;
            }
            catch (Exception ex)
            {
                string strex = ex.GetType().Name;
            }
            return result;


        }

    }
}
