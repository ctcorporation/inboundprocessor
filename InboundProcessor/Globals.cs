﻿using NodeResources;

namespace InboundProcessor
{
    public static class Globals
    {
        private static IMailServerSettings _mailServerSettings
        { get; set; }
        private static string _appPath { get; set; }

        private static string _winSCPLocation { get; set; }
        private static string _tempLocation { get; set; }
        private static string _archiveLocation { get; set; }
        private static string _primaryLocation { get; set; }
        private static string _testTempLocation { get; set; }
        private static string _testArchiveLocation { get; set; }
        private static string _testPrimaryLocation { get; set; }
        private static string _testPickupLocation { get; set; }
        private static string _pickupLocation { get; set; }
        private static string _outboundLocation { get; set; }
        private static string _testOutboundLocation { get; set; }
        private static string _ReProcessLocation { get; set; }
        private static string _testReProcessLocation { get; set; }
        private static int _pollTime { get; set; }
        private static int _refeshTime { get; set; }

        private static NodeData.IConnectionManager _connString { get; set; }
        private static string _appConfig
        {
            get; set;
        }

        public static IMailServerSettings MailServerSettings
        {
            get { return _mailServerSettings; }
            set { _mailServerSettings = value; }
        }

        public static string TempLocation
        {
            get { return _tempLocation; }
            set { _tempLocation = value; }
        }
        public static string ArchiveLocation
        {
            get { return _archiveLocation; }
            set { _archiveLocation = value; }
        }
        public static string PrimaryLocation
        {
            get { return _primaryLocation; }
            set { _primaryLocation = value; }
        }
        public static string TestPickupLocation
        {
            get { return _testPickupLocation; }
            set { _testPickupLocation = value; }
        }
        public static string PickupLocation
        {
            get { return _pickupLocation; }
            set { _pickupLocation = value; }
        }
        public static string OutboundLocation
        {
            get { return _outboundLocation; }
            set { _outboundLocation = value; }
        }
        public static string TestOutboundLocation
        {
            get { return _testOutboundLocation; }
            set { _testOutboundLocation = value; }
        }
        public static string TestTempLocation
        {
            get { return _testTempLocation; }
            set { _testTempLocation = value; }
        }
        public static string TestArchiveLocation
        {
            get { return _testArchiveLocation; }
            set { _testArchiveLocation = value; }
        }
        public static string TestPrimaryLocation
        {
            get { return _testPrimaryLocation; }
            set { _testPrimaryLocation = value; }
        }

        public static string ReProcessLocation
        {
            get
            { return _ReProcessLocation; }
            set { _ReProcessLocation = value; }
        }

        public static string TestReProcessLocation
        {
            get
            {
                return _testReProcessLocation;
            }
            set
            { _testReProcessLocation = value; }
        }
        public static string AppPath
        {
            get { return _appPath; }
            set { _appPath = value; }
        }

        public static string WinSCPLocation
        {
            get
            { return _winSCPLocation; }
            set
            {
                _winSCPLocation = value;
            }
        }


        public static int PollTime
        {
            get { return _pollTime; }
            set { _pollTime = value; }
        }

        public static int RefreshTime
        {
            get { return _refeshTime; }
            set { _refeshTime = value; }
        }

        public static string AppConfig
        {
            get { return _appConfig; }
            set { _appConfig = value; }
        }
        public static NodeData.IConnectionManager ConnString
        {
            get { return _connString; }
            set { _connString = value; }
        }

        public static string AlertsTo { get; internal set; }
        public static string AppLogPath { get; internal set; }
    }
}
